//$Id$
package desksocket;

import java.util.HashMap;


import desksocket.RequestDataType.RequestOrigin;
import desksocket.SessionThreadLocal.SessionInfo;
import desksocket.WSFrame.FrameType;

public class WSSession implements SessionInterface {

    private ProtocolConstant pc;
    long sessionId;
    long sessionId1;
    private boolean greet = false;

    public WSSession(ProtocolConstant pc, long sessionId) {
        this.sessionId = sessionId;
        this.pc = pc;
        this.sessionId1 = 0l;
    }

    @Override
    public void initialize() throws Exception {
        // Session specific websocket Handlers
      //  System.out.println("A new client is connected with  a sessionId : " + sessionId + " adding specific handlers");
        StaticServer.getObj().addWebsocketHandlers(ProtocolConstants.WEBSOCKET, sessionId,
                new WSHandshakehandler());
      //  System.out.println("Session Initialize call done");
    }

    @Override
    public long getTimeout() {
        // TODO Auto-generated method stub
        return 30 * 60 * 1000;
    }

    @Override
    public boolean handleRequestInternal(Object req, RequestOrigin reqOrigin) throws Exception {
    	//	System.out.println("handleRequestInternal  called ");
        if (!greet) {
          //  System.out.println("Recieved Greet");
            greet = true;
            return false;
        }
        String content = (String) req;
        if (content.endsWith("-")) {
         //   System.out.println("Recieved Stay alive message");
        } else {
        //    System.out.println("Received msg is " + content);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(getUser(), content);
        StaticServer.getObj().getOutputStream(sessionId, ProtocolConstants.WEBSOCKET)
                .writeObject(new WSFrame(FrameType.TEXT, content.getBytes())); //Writing happens Here
        return false;
    }

    @Override
    public Object getProtocol() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean secureCurrentSession() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void throwFlushException(String msg) {
        System.out.println(msg);
    }

    @Override
    public void lazyInitialize() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void processIOException() {
        System.out.println("Exception occured");
    }

    @Override
    public byte[] getGreetString() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void throwWriteException(Exception ex) {
        ex.printStackTrace();
        System.out.println(ex.getMessage());
    }

    @Override
    public void checkConnectionAllowed() throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleProcessingException(Exception ex, Long sessionId) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getUser() {
        // TODO Auto-generated method stub
        return "abi";
        // return ZMLibraryComponent.getServerAPI().getUser(sessionId, pc);
    }

    @Override
    public void setSessionInfoInternal(SessionInfo sessionInfo) {
        // TODO Auto-generated method stub

    }

    @Override
    public SessionInfo getSessionInfoInternal() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void sendBye() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCommandName(Object reqObject) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getCommand(Object reqObject) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void writeState(boolean write) {
        // TODO Auto-generated method stub

    }

}
