//$Id$
package desksocket;

public interface RequestDataType {
    int getType();

    Long getSessionId();
    
    RequestOrigin getRequestOrigin();
    
    enum RequestOrigin {
        EXTERNAL,
        INTERNAL;
    }
}
