//$Id$
package desksocket;

import java.util.Map;

public class HttpResponseObject  implements ResponseObject {

    private HttpResponseStatusline status;
    // content length header is Mandatory
    private Map<String, String> headers;
    private ResponseContent content;

    
    public HttpResponseObject(HttpResponseStatusline status, Map<String , String> headers, ResponseContent content) {
        this.status = status;
        this.headers = headers;
        this.content = content;
    }
    
    public HttpResponseStatusline getStatus() {
        return status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public ResponseContent getContent() {
        return content;
    }
    
    public int getLength() {
        if (content == null) {
            return 0;
        }
        return content.getLength();
    }


}
