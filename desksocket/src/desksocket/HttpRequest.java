//$Id$
package desksocket;

import java.util.Map;

public class HttpRequest {

    private byte[] content;
    private String method;
    private String uri;
    private Map<String, String> headers;
    private String ip;
    
    public HttpRequest(byte[] buf,String method , String uri , Map<String,String> headers , String ip) {
      this.content = buf;
      this.method = method;
      this.uri = uri;
      this.headers = headers;
      this.ip = ip;
    }

    public String getIp() {
        return ip;
    }
    
    public byte[] getContent() {
        return content;
    }
    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }
    public Map<String, String> getHeaders() {
        return headers;
    }
}
