//$Id$
package desksocket;

import java.io.Serializable;

public class RequestData implements Serializable, RequestDataType {
    private static final long serialVersionUID = 1L;

    public static final int READPACKET = 1;
    public static final int WRITEPACKET = 2;
    /** The client had close the connection. Do the necessary **/
    public static final int FINPACKET = 3;
    /** The server had closed or ready to close connection. Do the necessary **/
    public static final int CLOSEPACKET = 4;

    private int type = 1;
    private final Long sessionId;
    private final Object request;

    private RequestOrigin reqOrigin;
    private boolean parseStatus = true;

    public RequestData(Long sessionId, int type, Object request) {
        this(sessionId, type, request, true);
    }
    
    public RequestData(Long sessionId, int type, Object request, boolean parseStatus) {
        this(sessionId, type, request, parseStatus, RequestOrigin.EXTERNAL);
    }
    
    public RequestData(Long sessionId, int type, Object request,
            boolean parseStatus, RequestOrigin reqOrigin) {
        this.sessionId = sessionId;
        this.type = type;
        this.request = request;
        this.parseStatus = parseStatus;
        this.reqOrigin = reqOrigin;
    }

    @Override
    public int getType() {
        return this.type;
    }

    @Override
    public Long getSessionId() {
        return this.sessionId;
    }
    
    @Override
    public RequestOrigin getRequestOrigin() {
        return reqOrigin;
    }

    public Object getReqObject() {
        return this.request;
    }

    public boolean getParseStatus() {
        return this.parseStatus;
    }

    @Override
    public String toString() {
        // return new String(data);
        if (request != null) {
            return "RequestData : type  : [" + type + "] : session : " + sessionId + " : data Length : " + request;
        }
        return "RequestData : type  : [" + type + "] : session : " + sessionId;
    }

}
