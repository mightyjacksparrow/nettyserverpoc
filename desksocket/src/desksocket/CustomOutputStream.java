//$Id$
package desksocket;

import java.io.OutputStream;

public abstract class CustomOutputStream extends OutputStream {
    public abstract void write(byte[] data, int off, int len);
    
    public abstract void write(byte[] data);
    
    public abstract void write(int data);
    
    public abstract void flush();
    
    public abstract void writeObject(ResponseObject obj);
    
    public abstract void writeObject(ResponseObject obj , int len);

}
