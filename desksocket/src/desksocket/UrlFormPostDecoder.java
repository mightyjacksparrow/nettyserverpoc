//$Id$
package desksocket;

import java.io.ByteArrayOutputStream;

import io.netty.handler.codec.http.HttpContent;

public class UrlFormPostDecoder implements PostBodyDecoder {

    private ParseState state = ParseState.FIND_KEY;
    private long readbytes;
    private int contentlen;
    private long maxBuffersize = 10 * 1024; // 10KB

    private String key;
    private int searchIdx;
    private ByteArrayOutputStream keybaos = new ByteArrayOutputStream();
    private ByteArrayOutputStream valuebaos = new ByteArrayOutputStream();

    enum ParseState {
        FIND_KEY, FIND_VAL, END;
    }

    public UrlFormPostDecoder(int len) {
        this.contentlen = len;
    }

    public RequestContent getDecodedContent(HttpContent httpcon) throws Exception {
        while (searchIdx < httpcon.content().writerIndex()) {

            bufferOverflow();
            switch (state) {
            case FIND_KEY:
                if (httpcon.content().getByte(searchIdx) == '=') {
                    key = new String(keybaos.toByteArray());
                    keybaos = new ByteArrayOutputStream();

                    state = ParseState.FIND_VAL;
                } else {
                    keybaos.write(new byte[] { httpcon.content().getByte(searchIdx) });
                }
                break;
            case FIND_VAL:
                if (httpcon.content().getByte(searchIdx) == '&') {
                    httpcon.content().readerIndex(searchIdx + 1);
                    state = ParseState.FIND_KEY;
                    RequestContent reqCon = new RequestContent(key, null, false, true, valuebaos.toByteArray());
                    valuebaos = new ByteArrayOutputStream();
                    searchIdx++;
                    readbytes++;
                    return reqCon;
                } else {
                    valuebaos.write(new byte[] { httpcon.content().getByte(searchIdx) });
                    if (readbytes == contentlen - 1) {
                        httpcon.content().readerIndex(searchIdx + 1);
                        RequestContent reqCon = new RequestContent(key, null, false, true, valuebaos.toByteArray());
                        valuebaos = new ByteArrayOutputStream();
                        searchIdx++;
                        readbytes++;
                        return reqCon;

                    }
                }
                break;
            default:
                throw new Exception("Illegal State");
            }
            searchIdx++;
            readbytes++;
            httpcon.content().readerIndex(searchIdx);
        }

        // empty value for a key
        if (readbytes == contentlen) {
            if (state.equals(ParseState.FIND_VAL)) {
                RequestContent reqCon = new RequestContent(key, null, false, true, valuebaos.toByteArray());
                return reqCon;
            } else {
                throw new IllegalStateException("Content length differ");
            }
        }
        return null;
    }

    private void bufferOverflow() throws Exception {
        if (readbytes > contentlen) {
            throw new IllegalStateException("Content length differ " + contentlen + "  ReadBytes " + readbytes);
        }
        if (keybaos.size() + valuebaos.size() > maxBuffersize) {
            throw new Exception("Buffer OverFlow KeyBaos : " + keybaos.size() + " Value Baos : " + valuebaos.size());
        }
    }
}
