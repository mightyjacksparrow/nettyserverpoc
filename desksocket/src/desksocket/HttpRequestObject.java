//$Id$
package desksocket;

import java.util.HashMap;
import java.util.Map;

public class HttpRequestObject {

    private String method;
    private String uri;
    private Map<String , String> queryString;
    private Map<String, String> headers;
    private String ip;
    private long len;
    private boolean isreqover = false;
    private RequestContent reqCon;
    
    public HttpRequestObject(String method , String uri , Map<String,String> headers , String ip) {
      this.method = method;
      this.uri = uri.split("\\?")[0];
      this.headers = headers;
      this.ip = ip;
      this.queryString = new HashMap<>();
      if (uri.split("\\?").length > 1) {
          String qstring = uri.split("\\?")[1];
          for (String keyval : qstring.split("&")) {
               String key = "" ;
               String val = "";
               key = keyval.split("=")[0];
               if (keyval.split("=").length > 1) {
                   val = keyval.split("=")[1];
               }
               queryString.put(key, val);
          }
      }
    }

    public Map<String , String > getQueryStringParams() { 
        return this.queryString;
    }
    
    public void setLength (int len){
        this.len = len;
    }
    public void setIsOver(boolean isover) {
        this.isreqover = isover;
    }
    
    public RequestContent getContent() {
        return this.reqCon;
    }
    
    public void setContent(RequestContent content) {
        this.reqCon = content;
    }
    
    public String getIp() {
        return this.ip;
    }
    
    public String getMethod() {
        return this.method;
    }

    public String getUri() {
        return this.uri;
    }
    public Map<String, String> getHeaders() {
        return this.headers;
    }
    
    public boolean isRequestOver () {
        return this.isreqover;
    }
    
    public long getLength() {
        return this.len;
    }
}
