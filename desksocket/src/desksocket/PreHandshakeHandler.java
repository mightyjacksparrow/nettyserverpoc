//$Id$
package desksocket;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;


public class PreHandshakeHandler extends ChannelDuplexHandler {
    private static final Logger LOG = Logger.getLogger(PreHandshakeHandler.class.getName());

    private static final ThreadPoolExecutor EXEC = new ThreadPoolExecutor(0, 200, 3, TimeUnit.SECONDS,
            new SynchronousQueue<Runnable>(), new CustomThreadFactory("Prehandshake handler thread pool"));

    private String trustedOrigins = "";
    private WebSocketInterface handlerIf;
    private ChannelHandlerContext ctx;
    private HttpRequest request;
    private FullHttpRequest httpReq;

    public PreHandshakeHandler(WebSocketInterface wsIf) {
        this.handlerIf = wsIf;
    }

    private boolean originCheck() {
        HttpHeaders headers = this.httpReq.headers();
        for (String trusOrigin : trustedOrigins.split(",")) {
            if (headers.get("Origin") != null && headers.get("Origin").equals(trusOrigin)) {
                return true;
            }
            if (headers.get("origin") != null && headers.get("origin").equals(trusOrigin)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) throws Exception {
    		//System.out.println(" PreHandshakeHandler channelRead called ");
        FullHttpRequest req = (FullHttpRequest) obj;
        this.ctx = ctx;
        this.httpReq = req;
        byte[] content = new byte[req.content().readableBytes()];
        req.content().readBytes(content);
        String ip = ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();
        request = new HttpRequest(content, req.getMethod().toString(), req.getUri(), processReq(req), ip);
        this.httpReq.setUri(handlerIf.getWSPath());
        this.httpReq.retain();
        EXEC.submit(new PreHandShakeHandlerWorker(request));
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (!(msg instanceof FullHttpResponse)) {
            // it happens if the handler is not removed while the actual msg is sent
            // shouldnt happen
            throw new RuntimeException("Invalid Response is passed other than HTTP Response");
        }
      //  System.out.println("PreHandshakeHandler write called ");
        ctx.writeAndFlush(msg);
        ctx.pipeline().remove(PreHandshakeHandler.class);//we are removing PreHandshakeHandler once processed
        EXEC.submit(new PostHandShakeHandlerWorker((SocketChannel) ctx.channel()));
    }

    private void postPreProcess(ValidationResult res) throws Exception {
    		//System.out.println("PreHandshakeHandler postPreProcess called ");
        if (res == null) {
            ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR));
            ctx.close();
        } else {
            switch (res.getHttpResponseStatus()) {
            case OK:
                ByteBuf buf = ctx.alloc().buffer().writeBytes(res.getHttpResponse().getContent());
                DefaultFullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                        HttpResponseStatus.OK, buf);
                httpResponse.headers().set(CONTENT_TYPE, res.getHttpResponse().getContentType());
                httpResponse.headers().set(CONTENT_LENGTH, buf.readableBytes());
                ctx.writeAndFlush(httpResponse);
                ctx.close();
                break;
            case HTTP_FORBIDDEN:
                ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN));
                ctx.close();
                break;
            case HTTP_NOT_FOUND:
                ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND));
                ctx.close();
                break;
            case HTTP_INTERNAL_SERVER_ERROR:
                ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1,
                        HttpResponseStatus.INTERNAL_SERVER_ERROR));
                ctx.close();
                break;
            default:
          //  	System.out.println("PreHandshakeHandler postPreProcess default called ");
                ctx.fireChannelRead(httpReq);//During Handshake this is called
            }
        }
    }

    private HashMap<String, String> processReq(FullHttpRequest req) {
//Get Headers
        HashMap<String, String> map = new HashMap<String, String>();
        HttpHeaders headers = req.headers();
        Iterator<Entry<String, String>> it = headers.iterator();
        while (it.hasNext()) {
            Entry<String, String> entry = it.next();
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    private class PostHandShakeHandlerWorker implements Runnable {

        private SocketChannel sc;
        
        public PostHandShakeHandlerWorker(SocketChannel sc ) {
            this.sc = sc;
        }
        
        @Override
        public void run() {
        	 //  System.out.println(" PostHandShakeHandlerWorker called ");
            WSStatus status = handlerIf.postHandShakeProcess();
            if (!(status.equals(WSStatus.CONNECTION_SUCCESS))) {
                CloseWebSocketFrame closeFrame = new CloseWebSocketFrame(status.getStatusCode(), status.getMessage());
                sc.writeAndFlush(closeFrame);
                sc.close();
            }
        }
    }
    
    private class PreHandShakeHandlerWorker implements Runnable {

        private HttpRequest req;

        PreHandShakeHandlerWorker(HttpRequest req) {
            this.req = req;
        }

        @Override
        public void run() {
            ValidationResult res = null;
           // System.out.println("PreHandShakeHandlerWorker  called ");
            try {
                res = handlerIf.validateRequest(req);
            } catch (Exception e) {
                res = new ValidationResult(true, new HttpResponse(HttpResponseStatusline.HTTP_INTERNAL_SERVER_ERROR));
                LOG.log(Level.INFO, "Exception ", e);
            }
          //  System.out.println("postPreProcess from PreHandShakeHandlerWorkercalled ");
            try {
                postPreProcess(res);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
