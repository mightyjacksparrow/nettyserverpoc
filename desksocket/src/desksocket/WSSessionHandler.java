//$Id$
package desksocket;

public class WSSessionHandler implements SessionHandlerInterface {

	ProtocolConstant pc;

	public WSSessionHandler(ProtocolConstant pc) {
		this.pc = pc;
	}

	@Override
	public SessionInterface createSessionInterfaceImpl(Long sessionId) throws Exception {
		return new WSSession(pc, sessionId);
	}

}