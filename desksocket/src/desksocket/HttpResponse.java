//$Id$
package desksocket;

public class HttpResponse {

    private String contentType;
    private HttpResponseStatusline status;
    private byte[] content;
    
    public HttpResponse(HttpResponseStatusline status) {
        this.status = status;
        this.content = new byte[0];
        this.contentType = "text/plain";
    }
    
    public HttpResponse(HttpResponseStatusline status,byte[] content , String contentType) {
        this.status = status;
        this.content = content;
        this.contentType = contentType;
        
    }
    
    public String getContentType() {
        return contentType;
    }
    
    public HttpResponseStatusline getStatus() {
        return status;
    }
    
    public byte[] getContent() {
        return content;
    }
    
}
