//$Id$
package desksocket;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.codec.http.websocketx.ContinuationWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
//decode from A to B
public class WebsocketCodec extends MessageToMessageCodec<WebSocketFrame, WSFrame> {

    private static final int FRAME_SIZE = 8 * 1024;
    
    @Override
    protected void encode(ChannelHandlerContext ctx, WSFrame msg,
            List<Object> out) throws Exception {
    	//converting to TextWebSocketFrame as understud by netty.
    	 //  System.out.println("WebsocketCodec encode called for writing data... ");
         WSFrame frame = (WSFrame) msg;
         int startidx = 0;
         int endidx = frame.getLength(); 
         
         if (frame.getLength() <= FRAME_SIZE) {
             ByteBuf buf = ctx.alloc().buffer(FRAME_SIZE).writeBytes(frame.getContent(),startidx,endidx); 
             out.add(new TextWebSocketFrame(true , 0 , buf));
         } 
         else {
             ByteBuf buf = ctx.alloc().buffer(FRAME_SIZE).writeBytes(frame.getContent(),startidx,FRAME_SIZE);
             startidx += FRAME_SIZE;
             endidx -= FRAME_SIZE;
             out.add(new TextWebSocketFrame(false , 0 , buf));
             
             while (endidx > FRAME_SIZE) {
                 buf = ctx.alloc().buffer(FRAME_SIZE).writeBytes(frame.getContent(),startidx,FRAME_SIZE);
                 out.add(new ContinuationWebSocketFrame(false, 0, buf));
                 startidx += FRAME_SIZE;
                 endidx -= FRAME_SIZE;
             }
             buf = ctx.alloc().buffer(FRAME_SIZE).writeBytes(frame.getContent(),startidx,endidx);
             out.add(new ContinuationWebSocketFrame(true, 0, buf));
         }

//        ByteBuf buf = ctx.alloc().buffer(12 * 1024).writeBytes(msg.getContent(),0,msg.getLength());
//        out.add(new TextWebSocketFrame(buf));
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, WebSocketFrame frame,
            List<Object> out) throws Exception {
  	  // System.out.println("WebsocketCodec decode called,converting frame to object.reading from socket... ");
        out.add(frame.content());
        frame.content().retain();//Retains the message instead of ignoring it
    }
}
