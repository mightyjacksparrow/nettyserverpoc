//$Id$
package desksocket;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufProcessor;

public class ByteBufWrapperImpl implements ByteBufWrapperInterface {
    private ByteBuf buf;

    public ByteBufWrapperImpl(ByteBuf buf) {
        this.buf = buf;
    }

    @Override
    public int getReadableBytes() {
        // TODO Auto-generated method stub
        return this.buf.readableBytes();
    }

    @Override
    public int getReaderIndex() {
        // TODO Auto-generated method stub
        return this.buf.readerIndex();
    }

    @Override
    public int getWriterIndex() {
        // TODO Auto-generated method stub
        return this.buf.writerIndex();
    }

    @Override
    public byte getByte(int index) {
        // TODO Auto-generated method stub
        return this.buf.getByte(index);
    }

    @Override
    public int searchByte(final byte content) {
        return buf.forEachByte(new ByteBufProcessor() {

            @Override
            public boolean process(byte value) throws Exception {
                return value != content;
            }
        });
    }

    @Override
    public int searchCRLF(int offset, int length) {
        return buf.forEachByte(offset, length, ByteBufProcessor.FIND_CRLF);
    }

    @Override
    public byte[] getByteArray(int length) {
        byte[] content = new byte[length];
        buf.readBytes(content);
        return content;
    }

    @Override
    public void setReaderIndex(int index) {
        this.buf.readerIndex(index);
    }

    @Override
    public void setWriterIndex(int index) {
        this.buf.writerIndex(index);
    }

    @Override
    public int searchDelims(int startidx, int length, final byte[] content) {
        return buf.forEachByte(startidx, length, new ByteBufProcessor() {

            @Override
            public boolean process(byte value) throws Exception {
                boolean ret = true;
                for (int i = 0; i < content.length && ret; ++i) {
                    ret = ret & (content[i] != value);
                }
                return ret;
            }
        });
    }

}
