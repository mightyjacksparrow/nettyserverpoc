//$Id$
package desksocket;

import java.util.logging.Logger;

public class SessionIDGenerator {
    // Frame Bit pattern
    // sfb ipc ipd time.sec seq
    // sfb - 4 bits Protocol Bit
    // ipc - 8 bits class c net
    // ipd - 8 bits class d net
    // time - 32 bits time in secs since 1970 - expires @ Oct 02, 12:36:39 IST
    // 2096
    // seq - 12 bits max 4096 / safe @ 3000
    // ----------------
    // total- 64 bits
    // ----------------

    private static Logger logger = Logger.getLogger(SessionIDGenerator.class.getName());
    private static long ipframe = -1;
    private static long timeseed = -1;
    private static long gridUID = -1;
    private static long seqCounter = 0;
    private static long timeslice = 1000L;
    private static long timefactor = 1;
    private static volatile boolean initialized = false;
    
    public static boolean initialize() throws Exception {
        try {
            String ip = java.net.InetAddress.getLocalHost().getHostAddress();
            String[] sub = ip.split("\\.");
            ipframe = ((Long.parseLong(sub[2]) << 8) | (Long.parseLong(sub[3]))) << 44;
            setupSeed();
            return true;
        } catch (Exception exp) {
            
            throw new Exception("Unable to initialize UIDGenerator " + exp.getMessage());
        }
    }

    public static boolean initialize(long ts, long tf) throws Exception {
        timeslice = ts;
        timefactor = tf;
        return initialize();
    }

    private static void setupSeed() throws Exception {
        
        long ti = System.currentTimeMillis();//ZMLibraryComponent.getLibraryUtil().getMicroSecondTime();
        ti = ti / timeslice;

        if (timeseed == ti) {
            try {
                Thread.sleep(timefactor * timeslice);
            } catch (Exception e) {
            }
            ti = System.currentTimeMillis();  //ZMLibraryComponent.getLibraryUtil().getMicroSecondTime();
            ti = ti / timeslice;

            if (timeseed == ti) {
                logger.severe("timeseed is same");
                throw new Exception("Unable to generate unique id");
            }
        }
        timeseed = ti;
        gridUID = ipframe | ((timeseed & 0xffffffffL) << 12);
        seqCounter = 0;
        logger.info("New gridUID " + gridUID + " " + Long.toHexString(gridUID));
    }

    protected static long getUniqueIdInternal() throws Exception {
        if (!initialized) {
            initialize();
            initialized = true;
        }
        if (seqCounter > 3000) {
            setupSeed();
        } else {
            seqCounter++;
        }
        return gridUID + seqCounter;
    }

    // The mask value has been changed for 4 to 7 as the actual mask will be
    // applied in SessionHandler according to port and ssl value
    private static long mask = 0xFL << 60;

    public static synchronized long getUniqueId() throws Exception {
        
        return ((getUniqueIdInternal() | mask)) ;
    }
}
