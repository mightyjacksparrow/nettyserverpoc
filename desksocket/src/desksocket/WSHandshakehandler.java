//$Id$
package desksocket;

public class WSHandshakehandler implements WebSocketInterface {

    @Override
    public String getWSPath() {
        return "/ws";
    }

    @Override
    public ValidationResult validateRequest(HttpRequest req) {
        return new ValidationResult(false, new HttpResponse(HttpResponseStatusline.UPGRADE));
    }

    @Override
    public WSStatus postHandShakeProcess() {
        return WSStatus.CONNECTION_SUCCESS;
    }
}
