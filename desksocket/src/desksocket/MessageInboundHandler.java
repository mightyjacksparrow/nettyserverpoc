//$Id$
package desksocket;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.Enumeration;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLException;

import desksocket.RequestDataType.RequestOrigin;
import io.netty.channel.AdaptiveRecvByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DecoderException;
import io.netty.util.AttributeKey;

public class MessageInboundHandler extends ChannelInboundHandlerAdapter {
	public static final AttributeKey<Long> ATTRIBUTE_KEY = AttributeKey.valueOf("sessionId");

	private static final Logger LOG = Logger.getLogger(MessageInboundHandler.class.getName());
	private String hostAddress = null;
	private Server server = null;
	private Long sessionId = null;
	
	private SessionHandler sessionHandler = null;

	public MessageInboundHandler(Server server) {
		this.server = server;
	}

	/*
	 * Channel active will be called only once when the connection is created.
	 * Subsequent calls to this handler direct to the
	 * channelRead->channelReadComplete methods
	 */
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
//		System.out.println();
//		System.out.println();
//		System.out.println();
		// https://stackoverflow.com/questions/25281124/netty-4-high-and-low-write-watermarks
		SocketChannel sc = (SocketChannel) ctx.channel();
		sc.config().setOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 32 * 1024);
		sc.config().setOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 8 * 1024);
		sc.config().setOption(ChannelOption.RCVBUF_ALLOCATOR, new AdaptiveRecvByteBufAllocator(4096, 16384, 65535));
		//System.out.println("Channel Active Called");
		// Adding close Listener to Close session Properly
		sc.closeFuture().addListener(new CloseListener());
		sessionHandler = server.getSessionHandler();
		try {
			sessionId = sessionHandler.createSession(server, sc);
		} catch (Exception e) {
			LOG.log(Level.WARNING, "Exception while creating session ", e);
			ctx.close();
			return;
		}

		ProtocolDecoder protocolDecoder = (ProtocolDecoder) sc.pipeline().get(ProtocolDecoder.class);
		if (protocolDecoder != null) {
		//	System.out.println("Add session Handler and session Id to Protocol Decoder added at Default");
			protocolDecoder.setSessionHandler(sessionHandler);
			protocolDecoder.setSessionId(sessionId);
		}

		hostAddress = sessionHandler.getSession(sessionId).getHostAddress();
		/* For sending the greet messages for unsecured connections */
		if (!sessionHandler.getSession(sessionId).isSecured) {
		//	System.out.println("First call, For Upgrade with dummy object and count 0");
			sessionHandler.getSession(sessionId).processReadMessages(new Object(), 0);
		}
		//System.out.println("Set sessioId as Param to ChannelHandlerContext Instatnce" + sessionId );
		ctx.channel().attr(MessageInboundHandler.ATTRIBUTE_KEY).set(sessionId);
		LOG.log(Level.SEVERE, "session {0} created successfully ", new Object[] { "" + sessionId });
		
//		Enumeration<Long> aa = StaticServer.getObj().getAllCurrentSessions(ProtocolConstants.WEBSOCKET);
//		while(aa.hasMoreElements()){
//			//System.out.println("Active sessions : " + aa.nextElement());
//		}
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object reqObj) throws Exception {
//		System.out.println();
//		System.out.println();
//		System.out.println();
		//LOG.log(Level.SEVERE, "data recieved successfully for session {0}", new Object[] { "" + sessionId });
	//	System.out.println("Channel Read Called");
		boolean parseStatus = true;
		if (ctx.channel().pipeline().get(ProtocolDecoder.class) != null) {
			parseStatus = ctx.channel().pipeline().get(ProtocolDecoder.class).getMustParse();
		}
	//	System.out.println("channeRead call, count as 2" + parseStatus);
		sessionHandler.getSession(sessionId).processReadMessages(reqObj, 2, parseStatus, RequestOrigin.EXTERNAL);
	}

	@Override
	public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
		if (ctx.channel().isWritable()) {
			sessionHandler.getSession(sessionId).releasewriteLock();
			// dummy object to call notify
			sessionHandler.getSession(sessionId).processReadMessages(new DummyObject(), 1, RequestOrigin.INTERNAL);
		}
	}

	// Dummy Object to trigger the writablity change
	// and exception cases to async pool
	public static class DummyObject {
	}

	/* This method should try to act as a sink for handling any exception */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable throwable) throws Exception {
		boolean mustCloseHere = true;
		Session session = server.getSessionHandler().getSession(sessionId);
		LOG.log(Level.SEVERE, "Exception sessionID " + sessionId);
		try {
			if (throwable instanceof RejectedExecutionException) {
				LOG.log(Level.SEVERE, "Exception while submitting task to pool, session expired : ", throwable);
				session.setSessionExpired();
				mustCloseHere = false;
			} else if (throwable instanceof ClosedChannelException) {
				if (session != null) {
					session.doAppCleanUp(false);
				}
				LOG.log(Level.INFO, "closedchannelexception ", throwable);
			} else if (throwable instanceof IOException) {
				IOException ioe = (IOException) throwable;
				if (ioe.getMessage() != null && ioe.getMessage().contains("Connection reset by peer")
						&& hostAddress != null
						// &&
						// LibraryPropertiesHandler.getFirewallPortMonitoringIps().contains(hostAddress)
						) {
					LOG.log(Level.INFO, "handleIOException",
							new Object[] { "Connection reset by peer " + hostAddress });
				} else if (!(ioe instanceof SSLException)) {
					LOG.log(Level.INFO, "handleIOException:{0} hostaddress:{1}",
							new Object[] { ioe, "" + hostAddress });
				}
				if (session != null) {
					session.doAppCleanUp(false);
				}
			} else if (throwable instanceof DecoderException) {

				Throwable cause = throwable.getCause();
				if (cause != null) {
					if (!(cause instanceof SSLException)) {
						LOG.log(Level.INFO, "handleIOException:{0} hostaddress:{1}",
								new Object[] { cause, "" + hostAddress });
					}
				} else {
					LOG.log(Level.INFO, "handleIOException:{0} hostaddress:{1}",
							new Object[] { throwable, "" + hostAddress });
				}
				if (session != null) {
					session.doAppCleanUp(false);
				}
			}

			else if (throwable instanceof Exception) {
				LOG.log(Level.INFO, "Exception ", throwable);
				if (session != null) {
					session.doAppCleanUp(true);
					mustCloseHere = false;
				}
			} else {
				LOG.log(Level.SEVERE, "error ", throwable);
			}
		} catch (RejectedExecutionException ex) {
			/*
			 * The RejectedExceutionException is repeated because there is a
			 * chance that this exception might happen when trying to submit a
			 * job to the thread pool during the above try block. Thus, the
			 * session object will be cleared by the cron thread.
			 */
			LOG.log(Level.SEVERE, "Exception while submitting task to pool ", ex);
			session.setSessionExpired();
			mustCloseHere = false;
		} catch (Exception ex) {
			LOG.log(Level.SEVERE, "Exception while exception handling ", ex);
			mustCloseHere = true;
		} finally {
			if (mustCloseHere) {
				ctx.close();
			}
		}
	}

	private class CloseListener implements ChannelFutureListener {

		@Override
		public void operationComplete(ChannelFuture future) throws Exception {
			// completed successfully
			if (future.isSuccess() && future.isDone()) {
				if (sessionId != null) {
					//System.out.println("Close Session Called from CloseListener ");
					Session session = server.getSessionHandler().getSession(sessionId);
					if (session != null) {
						session.setChannelClose();
						/*
						 * Client closed the connection or session was not
						 * created properly. i.e. Exception during session
						 * creation in channelActive() method.
						 */
						LOG.log(Level.INFO, "client closed {0}", new Object[] { "sessionId :" + sessionId });
						session.processReadMessages(null, -1);
					}
				}
			} else if (future.cause() != null && future.isDone()) {
				LOG.log(Level.SEVERE, "Exception while closing ", future.cause());
				if (sessionId != null) {
					server.getSessionHandler().removeSession(sessionId);
				}
			}
		}

	}

}
