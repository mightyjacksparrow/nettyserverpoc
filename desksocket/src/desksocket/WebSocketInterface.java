//$Id$
package desksocket;

public interface WebSocketInterface {
    
    public String getWSPath();
    
    public ValidationResult validateRequest(HttpRequest req);

    public WSStatus postHandShakeProcess();
}
