//$Id$
package desksocket;

public interface ByteBufWrapperInterface {
	int getReadableBytes();

	int getReaderIndex();

	int getWriterIndex();

	byte getByte(int var1);

	void setReaderIndex(int var1);

	void setWriterIndex(int var1);

	int searchByte(byte var1);

	int searchCRLF(int var1, int var2);

	byte[] getByteArray(int var1);

	int searchDelims(int var1, int var2, byte[] var3);
}
