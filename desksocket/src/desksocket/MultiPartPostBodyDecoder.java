//$Id$
package desksocket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpContent;

public class MultiPartPostBodyDecoder implements PostBodyDecoder {

    private byte[] boundary = null;
    private long readBytes = 0;

    private long maxBufferSize = 20 * 1024; // 10KB [each chunk size is 16kb . So max buffer allowed is 20 kb]

    private byte[] backupBytes;

    // parsing variables;
    private int writeIdx = 0;
    private int searchIdx = 0;
    private int boundIdx = 0;
    private int closeIdx = 0;
    private boolean bodyheadernwline = false;
    private String bodyHeader = "";
    private boolean continouslinecheck = true;

    // state variables
    private MultiPartState state = MultiPartState.DELIM;

    // parse a boundary -- crlf boundary crlf
    private BoundaryParseState boundstate = BoundaryParseState.BOUNDARY;

    // requestVariables
    private Map<String, String> attributeMap = new HashMap<>();
    private boolean isFile = false;
    private String key = null;

    private ByteArrayOutputStream baos = new ByteArrayOutputStream();

    public MultiPartPostBodyDecoder(String contentTypeVal) {

        this.boundary = ("--" + contentTypeVal.split("=")[1]).getBytes();
    }

    public enum MultiPartState {
        DELIM, BODY_HEADER, BODY_PART, CLOSE_DELIM;
    }

    public enum BoundaryParseState {
        START_CRLF, BOUNDARY, CLOSE_BOUNDARY, END_CRLF;
    }

    /*
     * CRLF BOUNDARY CRLF
     * 
     * BODYPART HEADER CRLF CRLF DATA CRLF BOUNDARY CRLF
     */
    public RequestContent getDecodedContent(HttpContent con) throws Exception {
        RequestContent reqCon = null;
        searchIdx = con.content().readerIndex();
        writeIdx = -1;
        int idx = 0;
        int currnewlineIdx = -1;
        while (searchIdx < con.content().writerIndex()) {

            bufferOverflow();
            switch (state) {
            case DELIM:

                // Decoding started . Find boundary
                switch (boundstate) {
                case START_CRLF:
                    idx = findNewLine(con.content());
                    if (idx == -2) {
                        // CR found dont increment the writer idx
                        searchIdx++;
                        continue;
                    }
                    if (idx == -1) {
                        writeIdx = searchIdx;
                        writeToByteArrayStream(con.content(), writeIdx - con.content().readerIndex() + 1);
                        searchIdx++;
                        continue;
                    }
                    currnewlineIdx = idx;
                    continouslinecheck = true;
                    boundstate = BoundaryParseState.BOUNDARY;
                    break;
                case BOUNDARY:
                    // findNewline
                    // if new line then write newline Idx
                    // update the curr idx as newline idx
                    // state is boundary

                    // this check must be done only if previous byte is a
                    // newline
                    if (continouslinecheck) {
                        idx = findNewLine(con.content());
                        if (idx == -2) {
                            // CR found dont increment the writer idx
                            searchIdx++;
                            continue;
                        }
                        if (idx != -1) {
                            if (currnewlineIdx != -1) {
                                writeIdx = currnewlineIdx;
                            }
                            writeToByteArrayStream(con.content(), writeIdx - con.content().readerIndex() + 1);
                            currnewlineIdx = idx;
                            searchIdx++;
                            continue;
                        }
                    }

                    continouslinecheck = false;
                    if (boundary[boundIdx] != con.content().getByte(searchIdx)) {
                        writeIdx = searchIdx;
                        writeToByteArrayStream(con.content(), searchIdx - con.content().readerIndex() + 1);
                        boundIdx = 0;
                        boundstate = BoundaryParseState.START_CRLF;
                    } else {
                        boundIdx++;
                        // boundary found
                        // check for close boundary
                        if (boundIdx >= boundary.length) {
                            boundIdx = 0;
                            // found boundary so backup bytes not needed
                            backupBytes = null;
                            boundstate = BoundaryParseState.CLOSE_BOUNDARY;
                        }
                    }
                    break;
                case CLOSE_BOUNDARY:
                    if (con.content().getByte(searchIdx) == '-') {
                        closeIdx++;
                        // close delim found . Request parsing completed fully
                        if (closeIdx == 2) {
                            // negelect all remaining bytes
                            writeToByteArrayStream(con.content(), writeIdx - con.content().readerIndex() + 1);
                            con.content().skipBytes(con.content().readableBytes());
                            reqCon = new RequestContent(key, attributeMap, isFile, true, baos.toByteArray());
                            baos = new ByteArrayOutputStream();
                            state = null;
                            boundstate = BoundaryParseState.START_CRLF;

                            // file props
                            attributeMap = new HashMap<>();
                            isFile = false;
                            key = null;

                            closeIdx = 0;
                            // Debug to find total length(readBytes);
                            return reqCon;
                        }
                    }
                    // boundary occured must be negelected
                    else if (closeIdx > 0) {
                        // should not occur ' ----xx-a '
                        writeToByteArrayStream(con.content(), searchIdx - con.content().readerIndex() + 1);
                    }
                    // not a close Boundary . But Boundary found
                    else {
                        boundstate = BoundaryParseState.END_CRLF;
                    }
                    break;
                case END_CRLF:
                    // found boundary Now check for a newline
                    idx = findNewLine(con.content());
                    if (idx == -2) {

                        searchIdx++;
                        continue;
                    }
                    if (idx != -1) {
                        writeToByteArrayStream(con.content(), writeIdx - con.content().readerIndex() + 1);
                        state = MultiPartState.BODY_HEADER;
                        // boundary found if reqcon is set then pass the written
                        // bytearray
                        boundstate = BoundaryParseState.START_CRLF;
                        if (baos.size() > 0) {
                            reqCon = new RequestContent(key, attributeMap, isFile, true, baos.toByteArray());
                            baos = new ByteArrayOutputStream();

                            attributeMap = new HashMap<>();
                            isFile = false;
                            key = null;

                            return reqCon;
                        }
                    } else {
                        writeIdx = searchIdx;
                        writeToByteArrayStream(con.content(), searchIdx - con.content().readerIndex() + 1);
                    }
                    boundstate = BoundaryParseState.START_CRLF;
                    break;
                default:
                    break;
                }
                break;
            case BODY_HEADER:
                // find first newline
                if (!bodyheadernwline) {
                    idx = findNewLine(con.content());
                    // new line between each header content-dispostion ,
                    // contentType;

                    if (idx < 0) {
                        if (idx == -1) {
                            bodyHeader += (char) con.content().getByte(searchIdx);
                            bodyheadernwline = false;
                        }
                        searchIdx++;
                        continue;
                    }
                    // split header with delimiter ';'
                    String[] keyvals = bodyHeader.split(";");
                    for (String keyval : keyvals) {
                        if (keyval.contains("=")) {
                            attributeMap.put(keyval.split("=")[0].trim(), keyval.split("=")[1].trim());
                        } else if (keyval.contains(":")) {
                            attributeMap.put(keyval.split(":")[0].trim(), keyval.split(":")[1].trim());
                        }
                    }

                    // set reqContent properties
                    if (attributeMap.keySet().contains("filename")) {
                        // the upload part is File
                        String fileName = attributeMap.get("filename");
                        this.key = fileName.substring(1, fileName.length() - 1);
                        this.isFile = true;
                    } else {
                        this.key = attributeMap.get("name");
                        this.isFile = false;
                    }

                    bodyHeader = "";
                    bodyheadernwline = true;
                    searchIdx++;
                    continue;
                }
                // search for a emptyLine . If found the bodypart header is over
                idx = findNewLine(con.content());
                if (idx < 0) {
                    // if character searchidx should not be incremented as the
                    // char is part of header
                    if (idx == -1) {
                        bodyheadernwline = false;
                        continue;
                    }
                    searchIdx++;
                    continue;
                }
                writeIdx = searchIdx + 1;
                state = MultiPartState.DELIM;
                bodyheadernwline = false;
                bodyHeader = "";
                con.content().readerIndex(searchIdx + 1);
                break;

            default:
                throw new Exception("Multipart Decoding Failed with unknown State");
            }
            searchIdx++;
        }
        if (writeIdx > con.content().readerIndex()) {
            writeToByteArrayStream(con.content(), writeIdx - con.content().readerIndex() + 1);
        }

        if (baos.size() > 0) {
            reqCon = new RequestContent(key, attributeMap, isFile, false, baos.toByteArray());
            baos = new ByteArrayOutputStream();
        }
        if (con.content().readableBytes() > 0) {
            // write to a byte[]
            // it will be a new line
            // or a boundary element
            backupBytes = new byte[con.content().readableBytes()];
            con.content().readBytes(backupBytes);
        }
        return reqCon;
    }

    private void bufferOverflow() throws Exception {
        int size = 0;
        if (backupBytes != null) {
            size += backupBytes.length;
        }
        size += baos.size();
        size += bodyHeader.length();

        if (size > maxBufferSize) {
            throw new Exception("Buffer Overflow BAOS size : " + baos.size() + " body header size "
                    + bodyHeader.length());
        }
    }

    private void writeToByteArrayStream(ByteBuf content, int length) throws IOException {
        if (backupBytes != null) {
            baos.write(backupBytes);
            backupBytes = null;
        }
        byte[] temp = new byte[length];
        content.readBytes(temp);
        baos.write(temp);
        readBytes += temp.length;
    }

    private int findNewLine(ByteBuf buf) {
        if (buf.getByte(searchIdx) == '\r') { // CR
            return -2;
        }
        if (buf.getByte(searchIdx) == '\n') { // LF
            return searchIdx;
        }
        return -1;
    }
}
