//$Id$
package desksocket;

public enum WSStatus {

    INTERNAL_ERROR("Internal Server Error",1011),
    CLOSE_UNSUPPORTED("Unsupported Format",1003),
    POLICY_VIOLATION("Connection Policy Violated",1008),
    CLOSE_TOO_LARGE("Too Large Message Recieved",1009),
    AUTHENTICATION_FAILED("Authenticate Failed",4001),
    CONNECTION_OVERLIMIT("Too Many Connections",4002),
    CONNECTION_SUCCESS("Connection Success",4003),
    TOO_MANY_USER_SESSIONS("Too many active sessions for user " ,4004),
    AUTHORIZATION_FAILED("User authorize failed",4005),
    EMPTY_TICKET("TP Ticket Not Set", 4006),
    ;
    
    private String message;
    private int statusCode;
    
    private WSStatus(String msg , int status) {
        this.message = msg;
        this.statusCode = status;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public int getStatusCode() {
        return this.statusCode;
    }
}
