//$Id$
package desksocket;

public enum HttpResponseStatusline {


    OK (200),
    METHOD_NOT_ALLOWED (405),
    SIZE_TOO_LARGE (200),
    HTTP_NOT_FOUND (404),
    HTTP_FORBIDDEN (403),
    HTTP_INTERNAL_SERVER_ERROR (500),
    HTTP_REDIRECT (303),
    UPGRADE (101);

    private int code;
    
    private HttpResponseStatusline(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return this.code;
    }
    
//    OK - 200
//    FORBIDDEN - 403
//    NOT_FOUND - 404
//    METHOD_NOT_ALLOWED - 405
//    REQUEST_ENTITY_TOO_LARGE - 413
//    INTERNAL_SERVER_ERROR - 500



}
