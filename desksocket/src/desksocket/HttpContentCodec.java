//$Id$
package desksocket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandler;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.DefaultLastHttpContent;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;

public class HttpContentCodec extends MessageToMessageCodec<Object, HttpResponseObject> implements
ChannelOutboundHandler {

    private static final Logger LOG = Logger.getLogger(HttpContentCodec.class.getName());

    private long sessionId;
    private ProtocolConstant pc;

    // used to parse the post body
    private PostBodyDecoder decoder;

    // request params
    private String reqmethod;
    private String uri;
    private String ip;
    private Map<String, String> headers;
    private String len;

    // used while writing response
    private boolean isHeadersSent = false;

    public HttpContentCodec(long sessionId, ProtocolConstant pc) {
    	  System.out.println("HttpContentCodec Initialized ");
        this.sessionId = sessionId;
        this.pc = pc;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object obj, ChannelPromise promise) throws Exception {
    	  System.out.println("HttpContentCodec write called ");  
    	HttpResponseObject msg = (HttpResponseObject) obj;
        updateLastAccessTime();
        DefaultHttpResponse httpResponse;
        HttpResponseStatusline resStatus = msg.getStatus();
        switch (resStatus) {
        case OK:
            if (!isHeadersSent) {
                isHeadersSent = true;
                httpResponse = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
                if (msg.getHeaders() != null) {
                    for (Entry<String, String> entry : msg.getHeaders().entrySet()) {
                        httpResponse.headers().set(entry.getKey(), entry.getValue());
                    }
                }
                ctx.writeAndFlush(httpResponse);
            }
            if (msg.getContent() != null) {
                ResponseContent con = msg.getContent();
                ByteBuf resContent = ctx.alloc().buffer(con.getLength()).writeBytes(con.getResponseContent());
                HttpContent content = new DefaultHttpContent(resContent);
                ctx.writeAndFlush(content);

                if (con.isFinal()) {
                    ctx.writeAndFlush(new DefaultLastHttpContent());
                    ctx.close();
                }
            } else {
                LOG.log(Level.INFO, "Null Response content in HttpResponse " + sessionId);
                ctx.writeAndFlush(new DefaultLastHttpContent());
                ctx.close();
            }
            break;
        case SIZE_TOO_LARGE:
            httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                    HttpResponseStatus.REQUEST_ENTITY_TOO_LARGE);
            ctx.writeAndFlush(httpResponse);
            ctx.close();
            break;
        case HTTP_FORBIDDEN:
            ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN));
            ctx.close();
            break;
        case HTTP_NOT_FOUND:
            ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND));
            ctx.close();
            break;
        case HTTP_INTERNAL_SERVER_ERROR:
            ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                    HttpResponseStatus.INTERNAL_SERVER_ERROR));
            ctx.close();
            break;
        case HTTP_REDIRECT:
            httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                    HttpResponseStatus.SEE_OTHER);
            if (msg.getHeaders() != null) {
                for (Entry<String, String> entry : msg.getHeaders().entrySet()) {
                    httpResponse.headers().set(entry.getKey(), entry.getValue());
                }
            }
            ctx.writeAndFlush(httpResponse);
            ctx.close();
            break;
        default:
            throw new Exception("illegal State ");
        }
    }

    private void updateLastAccessTime() {
      //  ZMLibraryComponent.getServerAPI().updateLastAccessTime(sessionId, pc);
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, HttpResponseObject msg, List<Object> out) throws Exception {
    	   System.out.println("HttpContentCodec encode called ");  
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, Object msg, List<Object> out) throws Exception {
        // reap connection case
        updateLastAccessTime();
        System.out.println("HttpContentCodec decode called ");  
        if (msg instanceof HttpRequest) {
            HttpRequest httpreq = (HttpRequest) msg;

             // malformed request
            if (httpreq.getUri().equals("/bad-request")) {
                ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
                ctx.close();
                return;
            }
            // get and post message processing
            this.reqmethod = httpreq.getMethod().name();
            this.uri = httpreq.getUri();
            this.headers = parseHeaders(httpreq);
            this.ip = getRemoteAddress();
            HttpRequestObject reqObj = new HttpRequestObject(this.reqmethod, this.uri, headers, this.ip);
            switch (this.reqmethod) {
            case "GET":
                // no content parsing for get request
                // so parsing over . pass it to next handler
                reqObj.setIsOver(true);
                out.add(reqObj);
                break;
            case "OPTIONS":
                //No content parsing for options method 
                //with respect to RFC 7231 . If payload is present,
                //it is of no use
                reqObj.setIsOver(true);
                out.add(reqObj);
                break;

            case "POST":
                // post file upload process
                // Request contains body
                // supported body content types are multipart/form-data and
                // url-form-encoded
                // TODO:for now if content length is not given give unsupported
                // response
                // if content-type is not as mentioned ... send error Response

                String contentType = httpreq.headers().get(HttpHeaders.Names.CONTENT_TYPE);
                len = httpreq.headers().get(HttpHeaders.Names.CONTENT_LENGTH);

                if (contentType == null || len == null) {
                    LOG.log(Level.INFO, "ContentType is Empty Or ContentLength is null Type : " + contentType
                            + " : Length : " + len);
                    ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND));
                    ctx.close();
                    return;
                }

                if (contentType.split(";")[0].equals("multipart/form-data")) {
                    decoder = new MultiPartPostBodyDecoder(contentType.split(";")[1]);
                } else if (contentType.equals("application/x-www-form-urlencoded")) {
                    // handle as Normal postReq ie no boundary check
                    // but filename cannot be obtained ???
                    // TODO: if no proper key value data is present
                    decoder = new UrlFormPostDecoder(Integer.parseInt(len));
                } else {
                    LOG.log(Level.INFO, "Unsupported ContentType Recieved : " + contentType);
                    ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND));
                    ctx.close();

                }
                break;
            default:
                LOG.log(Level.INFO, "Unsupported HttpMethods Recieved : " + reqmethod);
                ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND));
                ctx.close();
            }
        }
        if (msg instanceof HttpContent) {
            HttpContent httpCon = (HttpContent) msg;
            while (httpCon.content().readableBytes() > 0) {
                RequestContent content = decoder.getDecodedContent(httpCon);
                if (content == null) {
                    break;
                }
                HttpRequestObject reqObj = new HttpRequestObject(this.reqmethod, this.uri, headers, this.ip);
                reqObj.setLength(Integer.parseInt(len));
                reqObj.setContent(content);
                if (httpCon instanceof LastHttpContent && httpCon.content().readableBytes() == 0) {
                    reqObj.setIsOver(true);
                }
                out.add(reqObj);
            }
        }
    }

    private Map<String, String> parseHeaders(HttpRequest req) {
        Map<String, String> headerMap = new HashMap();
        for (Entry<String, String> header : req.headers()) {
            headerMap.put(header.getKey(), header.getValue());
        }
        return headerMap;
    }

    private String getRemoteAddress() {
    		return String.valueOf(Math.random());    
    	//return ZMLibraryComponent.getServerAPI().getRemoteAddress(sessionId, pc).getAddress().getHostAddress();
    }
}
