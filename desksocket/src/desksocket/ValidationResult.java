//$Id$
package desksocket;

public class ValidationResult {
    private boolean invalidReq;
    private HttpResponse res;
    
    public ValidationResult(boolean invalidReq, HttpResponse res) {
        this.invalidReq = invalidReq;
        this.res = res;
    }
    
    public HttpResponse getHttpResponse() {
        return res;
    }
    
    public boolean isInvalidReq() {
        return invalidReq;
    }
    
    public HttpResponseStatusline getHttpResponseStatus() {
        return res.getStatus();
    }
}
