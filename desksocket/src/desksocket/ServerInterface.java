//$Id$
package desksocket;

public interface ServerInterface {
	void initialize() throws Exception;

	ProtocolDecoderInterface getProtocolDecoder() throws Exception;
}
