//$Id$
package desksocket;

public interface ProtocolConstant {
    
    public String getName();
    
    public int getProtocolId(); //unique for each protocol
    
    public int getPort();

    public int getMask();

    public boolean isSecured();

    public boolean isTLSRequired();
    
    public int getMaxPoolSize();
    
    public int getSslInfoId();
    
    public int getRequestQueueSize(); // Max no. of request objects to be queued . 
                                      // If reached , read will stopped for the session

}
