//$Id$
package desksocket;

public class StartingPoint {
	public static void main(String[] args) {
		try {
			ContextManager.registerHandler(new TempCtxHandler());
		//	System.out.println("Main Method Called. -- Gonna Initiate Server");
			StaticServer.getObj().startServer(new WSServer(ProtocolConstants.WEBSOCKET),
					new WSSessionHandler(ProtocolConstants.WEBSOCKET), ProtocolConstants.WEBSOCKET);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static class TempCtxHandler implements ContextHandler {
		@Override
		public String get() {
			return null;
		}

		@Override
		public Object set(String userid) {
			return null;
		}

		@Override
		public Object setForQueue(String userid) {
			return null;
		}

		@Override
		public Object setUser(String user) {
			return null;
		}

		@Override
		public void reset(Object token) {
		}

		@Override
		public Object setContext(String user) {
			return null;
		}
	}
}
