//$Id$
package desksocket;

import java.util.Map;

public class RequestContent {

    private String key;
    private byte[] content;
    private Map<String, String> attributeMap ;
    private boolean isFile = false;
    private boolean isFinal = false;
    
    public RequestContent(String key , Map<String, String> attrMap , boolean isFile , boolean isFinal , byte[] con) {
        this.key = key;
        this.isFile = isFile;
        this.attributeMap = attrMap;
        this.isFinal = isFinal;
        this.content = con;
    }

    public boolean isFile() {
        return this.isFile;
    }
    
    public Map<String, String> getContentHeader() {
        return attributeMap;
    }

    public String getKey() {
        return key;
    }

    public byte[] getPartContent() {
        return content;
    }

    public boolean isFinal() {
        return isFinal;
    }
}
