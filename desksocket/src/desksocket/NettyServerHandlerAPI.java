//$Id$
package desksocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Enumeration;

import com.google.protobuf.MessageLite;

import desksocket.RequestDataType.RequestOrigin;

public interface NettyServerHandlerAPI {
	void startServer(ServerInterface var1, SessionHandlerInterface var2, ProtocolConstant var3) throws Exception;

	CustomOutputStream getOutputStream(long var1, ProtocolConstant var3);

	void closeSession(long var1, ProtocolConstant var3, boolean var4);

	InetSocketAddress getRemoteAddress(long var1, ProtocolConstant var3);

	InetSocketAddress getLocalAddress(long var1, ProtocolConstant var3);

	Object getProtocol(long var1, ProtocolConstant var3);

	long getCreationTime(long var1, ProtocolConstant var3);

	void startTlsSupport(long var1, ProtocolConstant var3);

	void addCompressionSupport(Encoder var1, long var2, ProtocolConstant var4);

	void addDecompressionSupport(Decoder var1, long var2, ProtocolConstant var4);

	void addWebsocketHandlers(ProtocolConstant var1, long var2, WebSocketInterface var4);

	void addProtoBufDecoder(long var1, ProtocolConstant var3, MessageLite var4);

	void removeProtBufDecoder(long var1, ProtocolConstant var3);

	void updateLastAccessTime(long var1, ProtocolConstant var3);

	void setSyncWrite(long var1, ProtocolConstant var3, boolean var4);

	void processReadMessages(long var1, ProtocolConstant var3, byte[] var4, RequestOrigin var5) throws IOException;

	SessionInterface getSession(long var1, ProtocolConstant var3);

	void addHttpHandlers(long var1, ProtocolConstant var3);

	Enumeration<Long> getAllCurrentSessions(ProtocolConstant var1);
}
