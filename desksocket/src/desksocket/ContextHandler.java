//$Id$
package desksocket;

public interface ContextHandler {
    
//To get currentContext
public String get();

//Set Current context with userid. @return previous context
public Object set(String userid);

//Set Current context with userid. @return previous context
public Object setForQueue(String userid);

//Set Current context with zuid. @return previous context
public Object setUser(String user);

//reset to prev Context
public void reset(Object token);

public Object setContext(String user);

}
