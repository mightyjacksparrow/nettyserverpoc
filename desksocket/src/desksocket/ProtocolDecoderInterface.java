//$Id$
package desksocket;

import java.util.List;

public interface ProtocolDecoderInterface {
	void parseContent(long var1, ByteBufWrapperInterface var3, List<Object> var4) throws Exception;

	boolean shouldEnableParse(Object var1);
}
