//$Id$
package desksocket;

public interface Task {
    void process();
    String getTaskName();
}
