//$Id$
package desksocket;

import java.util.Map;

public interface SessionThreadLocal {
    public long getOriginatorUserId();
    public void setOriginatorUserId(long userId);
    public SessionThreadLocal get();
    public void reset();
    public Map<String,SessionInfo> getAllSessionInfo();
    public void setAllSessionInfo(Map<String,SessionInfo> sessionInfo);
    public void insertInGlobalCache(String module, String key, CacheUnit data);
    public CacheUnit getFromGlobalCache(String module, String key);
    public CacheUnit removeFromGlobalCache(String module, String key);
    //public interface Session { }
    public interface SessionInfo {
        public String getSession();
        // This will return the SessionInfo object based on the given type
        // If it's a QUEUE_LEVEL then it will return the same object as it will
        // be serialised and stored in the Queue
        // If it is THREAD_LEVEL it will clone the object and return a new Instance
        // It will be handled at the implementation level whether to return the same
        // instance or cloned instance 
        public SessionInfo getSessionInfo(Type type);
    }    
    
    public enum Type {
        QUEUE_LEVEL,THREAD_LEVEL
    }
}
