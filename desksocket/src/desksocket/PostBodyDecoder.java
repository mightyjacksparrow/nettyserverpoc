//$Id$
package desksocket;

import io.netty.handler.codec.http.HttpContent;

public interface PostBodyDecoder {
    public RequestContent getDecodedContent(HttpContent con) throws Exception;

}
