//$Id$
package desksocket;

import desksocket.RequestDataType.RequestOrigin;
import desksocket.SessionThreadLocal.SessionInfo;

public interface SessionInterface {
    public void initialize() throws Exception;

    public long getTimeout();

    /**
     * Perform protocol related request processing in this method.
     * 
     * @return True if the present connection needs to be close. Else<br>
     *         False
     */
    public boolean handleRequestInternal(Object req , RequestOrigin origin) throws Exception;

    public Object getProtocol();

    public boolean secureCurrentSession();

    public void throwFlushException(String msg);

    public void lazyInitialize() throws Exception;

    public void processIOException();

    public byte[] getGreetString();

    public void throwWriteException(Exception ex);

    public void checkConnectionAllowed() throws Exception;

    public void handleProcessingException(Exception ex, Long sessionId);

    public String getUser();

    public void setSessionInfoInternal(SessionInfo sessionInfo);

    public SessionInfo getSessionInfoInternal();

    public void sendBye();
    
    public String getCommandName(Object reqObject);

    public String getCommand(Object reqObject);

    /**
     * this method notify the protocol to hold or resume write 
     * if @param write is false then application
     * socket write should be paused 
     *and when it is true the write must be resumed
     *NOTE: 
     *      it will be used only in case of 
     *      {@link CustomOutputStream#writeObject(ResponseObject)}
     *      if not, this method can be ignored
    */
    public void writeState(boolean write) ; 
}
