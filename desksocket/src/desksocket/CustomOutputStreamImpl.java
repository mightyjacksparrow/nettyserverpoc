//$Id$
package desksocket;

import io.netty.buffer.ByteBuf;

public class CustomOutputStreamImpl extends CustomOutputStream {
    private final Long sessionId;
    private Session session;
    private SessionHandler sessionHandler;

    public CustomOutputStreamImpl(Long sessionId, SessionHandler sessionHandler) {
        this.sessionId = sessionId;
        this.sessionHandler = sessionHandler;
    }

    @Override
    public void write(byte[] data) {
        write(data, 0, data.length);
    }

    @Override
    public void write(byte[] data, int off, int len) {
    	System.out.println(" CustomOutputStream write called");
        if (session == null) { 
            session = sessionHandler.getSession(sessionId);
            if (session == null) {
                throw new RuntimeException("session : " + sessionId
                        + " is already terminated while trying to write data ");
            }
        }
        session.writeData(data, off, len);
    }

    @Override
    public void write(int data) {
        byte[] arr = new byte[1];
        arr[0] = (byte) data;
        write(arr);
    }

    @Override
    public void flush() {
        if (session == null) {
            session = sessionHandler.getSession(sessionId);
            if (session == null) {
                throw new RuntimeException("session : " + sessionId
                        + " is already terminated while trying to write data ");
            }
        }
        ByteBuf buf = sessionHandler.getSession(sessionId).getByteBuf();
        if (buf.readableBytes() > 0) {
            session.checkAndFlush();
        }
    }

    @Override
    public void writeObject(ResponseObject object) {
      writeObject(object,object.getLength());
    }
    
    @Override
    public void writeObject(ResponseObject object , int len) {
      //	System.out.println(" CustomOutputStream writeObject called");
        if (session == null) {
            session = sessionHandler.getSession(sessionId);
            if (session == null) {
                throw new RuntimeException("session : " + sessionId
                        + " is already terminated while trying to write data ");
            }
        }
        session.notifyUnWritability();
        session.updateLastAccessTime();
        session.checkActiveAndWriteException();
        session.checkAndFlush(object);
    }
}
