//$Id$
package desksocket;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomThreadFactory implements ThreadFactory  {

    private String threadFactoryName;
    AtomicInteger threadCount = new AtomicInteger(0);

    public CustomThreadFactory(String threadFactoryName) {
        this.threadFactoryName = threadFactoryName;
    }

    @Override 
    public Thread newThread(Runnable runnable) {
        return new Thread(runnable,threadFactoryName + "-thread-" + threadCount.incrementAndGet());
    }
}
