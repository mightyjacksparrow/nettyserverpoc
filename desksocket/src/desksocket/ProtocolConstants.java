//$Id$
package desksocket;

public enum ProtocolConstants implements ProtocolConstant {

    WEBSOCKET();
    
    private String NAME;
    private int PORT;
    private int POOL_SIZE;
    
    private ProtocolConstants() {
        NAME  = "WEBSOCKET";
        PORT = 9100;
        POOL_SIZE = 100;
    }
    
    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return this.NAME;
    }

    @Override
    public int getProtocolId() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getPort() {
        // TODO Auto-generated method stub
        return this.PORT;
    }

    @Override
    public int getMask() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isSecured() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isTLSRequired() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getMaxPoolSize() {
        // TODO Auto-generated method stub
        return this.POOL_SIZE;
    }

    @Override
    public int getRequestQueueSize() {
        return 100;
    }

    @Override
    public int getSslInfoId() {
        // TODO Auto-generated method stub
        return 0;
    }



}
