//$Id$
package desksocket;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AsyncRequestProcessor {

    private static final long TTL = 1000L;// Time to live in mills

    private int maxpoolsize;
    private ThreadPoolExecutor executor = null;
    private final SessionHandler sessionHandler;

    AsyncRequestProcessor(SessionHandler sessionHandler, ProtocolConstant protocolConstant) {
        this.sessionHandler = sessionHandler;
        initPool(protocolConstant);
    }

    private void initPool(ProtocolConstant protocolConstant) {
        if (executor != null) {
            return;
        }
        maxpoolsize = protocolConstant.getMaxPoolSize();
        executor = new ThreadPoolExecutor(0, maxpoolsize, TTL, TimeUnit.MILLISECONDS,
                new SynchronousQueue<Runnable>(),
                new CustomThreadFactory("NettyAsyncRequestProcessor-" + protocolConstant.getName()));

    }

    public int getMaxPoolSize() {
        return maxpoolsize;
    }

    public void process(RequestData data) throws RejectedExecutionException, NullPointerException {
    		//System.out.println("New Async request submitted from Server.process method");
        executor.execute(new RequestProcessor(data));
    }

    private class RequestProcessor implements Runnable {
        private RequestData mdata = null;
        private RequestProcessor(RequestData data) {
            this.mdata = data;
        }
        @Override
        public void run() {
            Session currSession = null;
            try {
            		//System.out.println("Async request Execution Started for session "+ mdata.getSessionId() );
                currSession = sessionHandler.getSession(mdata.getSessionId());
                if (currSession == null) {
                    return;
                }
                if (mdata.getType() == RequestData.READPACKET) {
                    try {
                 //   	System.out.println("Async request Execution Started and ReadPacket Called" );
                        currSession.handleRequest();
                    } catch (Exception ex) {
                        currSession.handleProcessingException(ex, mdata.getSessionId());
                    } catch (Throwable t) {
                        currSession.closeSession(false);
                        throw t;
                    }
                } else if (mdata.getType() == RequestData.CLOSEPACKET) {
                    currSession.closeSession();
                } else if (mdata.getType() == RequestData.FINPACKET) {
                    currSession.closeSession(false);
                }
            } catch (Exception e) {
            }
        }
    }
}
