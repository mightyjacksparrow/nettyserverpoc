//$Id$
package desksocket;

import java.util.List;

public class WSServer implements ServerInterface {

	ProtocolConstants pc;

	public WSServer(ProtocolConstants pc) {
		this.pc = pc;
	}

	@Override
	public void initialize() throws Exception {
		System.out.println("Websocket server Intiatlized");
	}

	@Override
	public ProtocolDecoderInterface getProtocolDecoder() throws Exception {
		return new ProtocolDecoderInterface() {

			@Override
			public boolean shouldEnableParse(Object reqObject) {
				return false;
			}

			@Override
			public void parseContent(long sessionId, ByteBufWrapperInterface in, List<Object> out) throws Exception {
				//System.out.println("WSServer ProtocolDecoderInterface parseContent Called");
				out.add(new String(in.getByteArray(in.getReadableBytes())));//convert byte to String
			}
		};
	}





}
