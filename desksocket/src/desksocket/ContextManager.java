//$Id$
package desksocket;

public class ContextManager {
    
    private static ContextHandler contextHandler;
    
    public static synchronized void registerHandler(ContextHandler handler) {
        if (contextHandler == null) {
            contextHandler = handler;
            return;
        }
        throw new IllegalArgumentException("Context Handler already registered");
    }
    
    public static ContextHandler getHandler() {
        return contextHandler;
    }
}
