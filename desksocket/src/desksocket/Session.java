//$Id$
package desksocket;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLException;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

public class Session {
	public static final int WAIT_TIME = 45000;
	private static final Object USEREVENT = new Object();
	private static final Logger LOG = Logger.getLogger(Session.class.getName());
	protected boolean handshakeSuccess;
	protected boolean readable;
	protected boolean channelOpen;
	private ByteBuf buf;
	protected long createdTime = -1L;
	protected long readDataCount = 0L;
	protected long writeDataCount = 0L;
	private Object writeLock = new Object();
	private int requestQueueSize;
	private final AtomicBoolean sessionClosedInitiated = new AtomicBoolean(false);
	private boolean closeInitiated = false;
	private boolean expired = false;
	private boolean greeted = false;
	protected boolean lazyInit = false;
	protected boolean isSecured = false;
	protected Long sessionId = null;
	protected Server server = null;
	protected String hostAddress = null;
	protected final CustomOutputStream cout;
	protected SSLEngine ssl = null;
	protected SocketChannel sc = null;
	protected ConcurrentLinkedQueue<RequestData> pendingList = new ConcurrentLinkedQueue();
	private volatile boolean deRegisteredRead;
	private AtomicBoolean requestInProgress = new AtomicBoolean(false);
	private int recursionCount = 0;
	private volatile Exception writeException = null;
	private long lastAccessTime;
	private SessionThreadLocal.SessionInfo sessionInfo;
	private SessionInterface sessionInterface;
	private boolean syncWrite = false;
	private boolean sessionPause = false;
	private SslHandler sslHandler;
	private static final String APP_IP =  "APPP"; //UIDManagerUtil.getUIDManager().getUniqueIdentifier();

	public Session(SessionInterface sessionInterface, Long sessionId, SocketChannel sc, Server server) {
		//System.out.println("Session initialized with constructor");
		this.sessionInterface = sessionInterface;
		createdTime = System.currentTimeMillis();
		lastAccessTime = System.currentTimeMillis();
		channelOpen = sc.isActive();
		readable = true;
		handshakeSuccess = false;
		this.sessionId = sessionId;
		this.sc = sc;
		hostAddress = sc.remoteAddress().getAddress().getHostAddress();
		this.server = server;
		isSecured = server.getProtocolConstant().isSecured();
		buf = sc.alloc().buffer(1024);
		requestQueueSize = server.getProtocolConstant().getRequestQueueSize();
		if (isSecured) {
			addSSLSupport(false);
		}
		cout = new CustomOutputStreamImpl(this.sessionId, this.server.getSessionHandler());
	}

	protected void addSSLSupport(boolean starttls) {
		if (ssl != null) {
			return;
		}
		//		ssl = SSLManager.getInstance().createSSLEngine(sc.alloc(), server.getProtocolConstant().getSslInfoId());
		sc.pipeline().addFirst(new ChannelHandler[]{new SslHandler(ssl, starttls)});
		sslHandler = ((SslHandler) sc.pipeline().get(SslHandler.class));
		sslHandler.handshakeFuture().addListener(new SslHandshakeListener());
	}

	private void queueRequest(RequestData pkt) {
		pendingList.add(pkt);
		int size = pendingList.size();
		if (size >= requestQueueSize) {
			deNotifyReadEvent();
			deRegisteredRead = true;
			LOG.log(Level.SEVERE, "queueRequest",
					new Object[]{"Queue size reached max deRegisteredRead read sessionID : ", sessionId});
		}
	}

	private void deNotifyReadEvent() {
		sc.config().setAutoRead(false);
	}

	private void notifyReadEvent() {
		sc.config().setAutoRead(true);
	}

	public void close() {
		closeSession(true);
	}

	public void closeSession() {
		closeSession(true);
	}

	protected ByteBuf getByteBuf() {
		return buf;
	}

	private void lockSession() {
		updateLastAccessTime();
		checkActiveAndWriteException();
		boolean canTry = true;
		while ((!sc.isWritable()) && (isChannelOpen())) {
			if (!canTry) {
				throwFlushException("Unable to flush response in 45000 ms for session " + sessionId);
			}
			try {
				synchronized (writeLock) {
					LOG.log(Level.SEVERE, "waiting on writeLock for session : {0}", new Object[]{"" + sessionId});
					writeLock.wait(45000L);
					canTry = false;
				}
			} catch (InterruptedException localInterruptedException) {
			}
		}
	}

	protected void checkAndFlush(ResponseObject obj) {
		lockSession();
		//System.out.println(" Session checkAndFlush called");
		sc.writeAndFlush(obj).addListener(new WriteListener(obj.getLength()));//This will encode and write to websocket
	}

	protected void checkAndFlush() {
		lockSession();
		ByteBuf outContent = sc.alloc().buffer(buf.readableBytes());
		buf.readBytes(outContent);
		if (syncWrite) {
			sc.writeAndFlush(outContent).addListener(new WriteListener(outContent.readableBytes()))
			.syncUninterruptibly();
		} else {
			sc.writeAndFlush(outContent).addListener(new WriteListener(outContent.readableBytes()));
		}
		buf.clear();
	}

	public void closeSession(boolean flushBuffer) {
		// Byte code:
		
	}

	private Object switchContext() {
		if (getUser() != null) {
			return ContextManager.getHandler().setUser(getUser());
		}
		return null;
	}

	private void resetContext(Object token) {
		if (token != null) {
			ContextManager.getHandler().reset(token);
		}
	}

	protected void releasewriteLock() {
		synchronized (writeLock) {
			writeLock.notify();
		}
	}

	void notifyUnWritability() {
		if (!sc.isWritable()) {
			sessionPause = true;
			sessionInterface.writeState(false);
		}
	}

	void notifyWritability() {
		if (sc.isWritable()) {
			sessionPause = false;
			sessionInterface.writeState(true);
		}
	}

	void checkActiveAndWriteException() {
		if (writeException != null) {
			throwWriteException(writeException);
		} else if (!isChannelOpen()) {
			LOG.log(Level.SEVERE, "isChannelOpen false");
			throwWriteException(new ClosedChannelException());
		}
	}

	protected void writeData(byte[] data, int offset, int length) {
		//System.out.println("Session writeData called");
		updateLastAccessTime();
		checkActiveAndWriteException();
		LOG.log(Level.SEVERE, "writing content length : {0}", new Object[]{Integer.valueOf(length)});
		int off = offset;
		while (length >= buf.writableBytes()) {
			int orig = buf.writableBytes();
			buf.writeBytes(data, off, buf.writableBytes());
			off += orig;
			length -= orig;
			checkAndFlush();
		}
		if (length > 0) {
			buf.writeBytes(data, off, length);
		}
	}

	public void processReadMessages(Object reqObject, int count) throws IOException {
		processReadMessages(reqObject, count, RequestDataType.RequestOrigin.EXTERNAL);
	}

	public void processReadMessages(Object reqObject, int count, RequestDataType.RequestOrigin reqOrigin)
			throws IOException {
		processReadMessages(reqObject, count, true, reqOrigin);
	}

	public void processReadMessages(Object reqObject, int count, boolean parseStatus,
			RequestDataType.RequestOrigin reqOrigin) throws IOException {
		LOG.log(Level.FINE, "processReadMessages", new Object[]{reqObject, Integer.valueOf(count)});

		//System.out.println("ProcessReadMessagesCalled from Session Object ps " + parseStatus + "count" + count );
		RequestData pkt = null;
		if (reqObject != null) {
			pkt = new RequestData(sessionId, 1, reqObject, parseStatus, reqOrigin);
		} else if (count == -1) {//Close Connection Packet
			pkt = new RequestData(sessionId, 3, null);
		} else {
			pkt = new RequestData(sessionId, 4, null);
		}
		queueRequest(pkt);
		//System.out.println("processReadMessages requestInProgress value " + requestInProgress );
		if (requestInProgress.compareAndSet(false, true)) {
			server.process(pkt);
		}
		LOG.log(Level.FINE, "processReadMessages", new Object[]{"return"});
	}

	public void handleRequest() throws Exception {
		//System.out.println("Session.handleRequest Called for session "+ sessionId );
		doPreamble();
		greet();
		if (sessionPause) {
			notifyWritability();
		}
		Object token = null;
		RequestData pkt;
		//System.out.println("Session.handleRequest pendingList Size Before : " + pendingList.size());
		//Checking some data is Present or not in Pending List
		while (((pkt = (RequestData) pendingList.poll()) != null) && (!closeInitiated) && (!sessionPause)) {
			//System.out.println("Session.handleRequest while loop called " + pendingList.size() + " " + pkt.getType());
			try {
				if (getSessionInfo() != null) {
					//System.out.println("GetSession info  called ");
					setSessionInfoInternal(getSessionInfo());
				}
				if (token == null) {
					switchContext();
				}
				checkActiveAndWriteException();
				if (pkt.getType() == 1) {//Read Packet
					//System.out.println("Session ReadPacket called ");
					boolean needClose = false;
					needClose = handleRequestInternal(pkt.getReqObject(), pkt.getRequestOrigin());
					ProtocolDecoder pdecoder = (ProtocolDecoder) sc.pipeline().get(ProtocolDecoder.class);
					if (!needClose) {
						if (syncWrite) {
							syncWrite = false;
						}
						if ((pdecoder != null) && (!pdecoder.getMustParse()) && (!pkt.getParseStatus())) {
							pdecoder.setMustParse(true);
					//		System.out.println("Fired UserEvent");
							sc.pipeline().fireUserEventTriggered(USEREVENT);
						}
					}
					//	closeSession();
					LOG.log(Level.FINE, "handleRequest", new Object[]{sessionId + "NeedClose"});
					requestInProgress.set(false);
					return;
				} else {
					//System.out.println("Session handleRequest else part called ");
					label793 : if (!isCloseInitiated()) {
						if (pkt.getType() == 3) {
							closeSession(false);
						} else if (pkt.getType() == 4) {
							closeSession(true);
						}
						LOG.log(Level.FINE, "handleRequest", new Object[]{sessionId + " Packet type:" + pkt.getType()});
						return;
					}
				}
			} finally {
				try {
					setSessionInfo(getSessionInfoInternal());
				} catch (Exception e) {
					LOG.log(Level.INFO, "Unable to setSessionInfo " + sessionId, e);
				}
				resetContext(token);
				//				ZMLibraryComponent.getSessionThreadLocalHandler().get().reset();
			}
		}
		chkAndClearNewPacket();
	}

	public SessionThreadLocal.SessionInfo getSessionInfo() {
	//	System.out.println("Session Getting SessionInfo");
		return sessionInfo;
	}

	public void setSessionInfo(SessionThreadLocal.SessionInfo sessionInfo) {
		this.sessionInfo = sessionInfo;
	}

	public void setSyncWrite(boolean flag) {
		syncWrite = flag;
	}

	private void chkAndClearNewPacket() throws Exception {
	//	System.out.println("session chkAndClearNewPacket called");
		if ((!pendingList.isEmpty()) && (!sessionPause)) {
			if (!requestInProgress.compareAndSet(false, true)) {
				return;
			}
			if (recursionCount < 10) {
				recursionCount += 1;
				handleRequest();
				recursionCount -= 1;
			} else {
				RequestData pkt = (RequestData) pendingList.peek();
				server.process(pkt);
			}
		}
		if (deRegisteredRead) {
			deRegisteredRead = false;
			notifyReadEvent();
		}
	}

	public void updateLastAccessTime() {
		lastAccessTime = System.currentTimeMillis();
	}

	protected void doPreamble() throws Exception {
		if (lazyInit) {
			return;
		}
		checkConnectionAllowed();

		lazyInit = true;
	}

	protected final void greet() {
		if (greeted) {
			return;
		}
		try {
			byte[] str = getGreetString();
			if (str != null) {
				cout.write(str);
				cout.flush();
				LOG.log(Level.SEVERE, "greet", new Object[]{"dispatching greet for session " + sessionId});
			}
			greeted = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void doAppCleanUp(boolean flushBuffer) throws IOException {
		if (flushBuffer) {
			processReadMessages(null, -2);
		} else {
			sc.close();
			processReadMessages(null, -1);
		}
	}

	private boolean initiateSessionClose() {
		closeInitiated = true;
		return sessionClosedInitiated.getAndSet(true);
	}

	public boolean isCloseInitiated() {
		return closeInitiated;
	}

	public boolean isChannelOpen() {
		return channelOpen;
	}

	public void setChannelClose() {
		channelOpen = false;
	}

	public boolean isReadable() {
		return readable;
	}

	public boolean isSessionExpired() {
		return expired;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public CustomOutputStream getCustomOutputStream() {
		return cout;
	}

	public SocketChannel getSocketChannel() {
		return sc;
	}

	public boolean getHandShakeDone() {
		return handshakeSuccess;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public long getCreationTime() {
		return createdTime;
	}

	public Server getHandler() {
		return server;
	}

	public void addReadDataCount(int count) {
		readDataCount += count;
	}

	public void addWriteDataCount(int count) {
		writeDataCount += count;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public WriteListener createtWriteListener(int len) {
		return new WriteListener(len);
	}

	public void setSessionExpired() {
		expired = true;
	}

	public void initialize() throws Exception {
		sessionInterface.initialize();
	}

	public void sendBye() {
		sessionInterface.sendBye();
	}

	public long getTimeout() {
		return sessionInterface.getTimeout();
	}

	public boolean handleRequestInternal(Object req, RequestDataType.RequestOrigin origin) throws Exception {
		return sessionInterface.handleRequestInternal(req, origin);
	}

	public Object getProtocol() {
		return sessionInterface.getProtocol();
	}

	public boolean secureCurrentSession() {
		return sessionInterface.secureCurrentSession();
	}

	public void processIOException() {
		sessionInterface.processIOException();
	}

	protected void throwFlushException(String msg) {
		sessionInterface.throwFlushException(msg);
	}

	protected void lazyInitialize() throws Exception {
		sessionInterface.lazyInitialize();
	}

	protected byte[] getGreetString() {
		return sessionInterface.getGreetString();
	}

	protected void throwWriteException(Exception ex) {
		sessionInterface.throwWriteException(ex);
	}

	protected void checkConnectionAllowed() throws Exception {
		sessionInterface.checkConnectionAllowed();
	}

	protected void handleProcessingException(Exception ex, Long sessionId) {
		sessionInterface.handleProcessingException(ex, sessionId);
	}

	protected String getUser() {
		return sessionInterface.getUser();
	}

	protected void setSessionInfoInternal(SessionThreadLocal.SessionInfo sessionInfo) {
		sessionInterface.setSessionInfoInternal(sessionInfo);
	}

	protected SessionThreadLocal.SessionInfo getSessionInfoInternal() {
		return sessionInterface.getSessionInfoInternal();
	}

	protected String getCommandName(Object reqObject) {
		return sessionInterface.getCommandName(reqObject);
	}

	private class SslHandshakeListener implements GenericFutureListener<Future<Channel>> {
		private SslHandshakeListener() {
		}

		public void operationComplete(Future<Channel> future) throws Exception {
			if ((future.isSuccess()) && (future.isDone())) {
				Session.LOG.log(Level.SEVERE, "Handshake success");
				if ((sslHandler.engine().getSession().getProtocol() != null)
						&& ((sslHandler.engine().getSession().getProtocol().contains("TLSv1"))
								|| (sslHandler.engine().getSession().getProtocol().contains("TLSv1.1")))) {
					Session.LOG.log(Level.INFO, "Getting SSL protocol version : "
							+ sslHandler.engine().getSession().getProtocol() + " session : " + sessionId);
				}
				handshakeSuccess = true;

				processReadMessages(new Object(), 0);
			} else if ((future.cause() != null) && (future.isDone())) {
				handshakeSuccess = false;
				Exception ex = (Exception) future.cause();
				Session.LOG.log(Level.SEVERE, "ssl handshake error : ", ex);
				sc.pipeline().fireExceptionCaught(new SSLException(ex));
			}
		}
	}

	private class WriteListener implements ChannelFutureListener {
		private int dataLength;

		public WriteListener(int dataLength) {
			this.dataLength = dataLength;
		}

		public void operationComplete(ChannelFuture future) {
		//	System.out.println("Session channelFutute operationComplete WriteListener called");
			if ((future.isSuccess()) && (future.isDone())) {
				addWriteDataCount(dataLength);
				Session.LOG.log(Level.SEVERE, "write success");
			} else if ((future.cause() != null) && (future.isDone())) {
				if (writeException != null) {
					return;
				}
				writeException = ((Exception) future.cause());
				try {
					Object dummyPacket = new Object();
					if (!closeInitiated) {
						processReadMessages(dummyPacket, 3);
					}
				} catch (Exception e) {
					sc.pipeline().fireExceptionCaught(e);
				}
			}
		}
	}

	//	private ProtocolStats getStatsType() {
	//		String type = server.getProtocolConstant().getName();
	//		if (type.contains("IMAP")) {
	//			return ProtocolStats.IMAP_STATS;
	//		}
	//		if (type.contains("POP")) {
	//			return ProtocolStats.POP_STATS;
	//		}
	//		if (type.contains("SMTPIN")) {
	//			return ProtocolStats.SMTP_IN_STATS;
	//		}
	//		if (type.contains("SMTPOUT")) {
	//			return ProtocolStats.SMTP_OUT_STATS;
	//		}
	//		return null;
	//	}

	//	private void addToStats(String command, long timeTaken) {
	//		try {
	//			ProtocolStats category = getStatsType();
	//			if ((category != null) && (command != null) && (!command.isEmpty())) {
	//				String zuid = getUser();
	//				if (zuid != null) {
	//					ZMLibraryComponent.getStatsQueue()
	//							.addToStatsQueue(new KeyValueStatsInfo(category, zuid + "", command, timeTaken));
	//				}
	//				ZMLibraryComponent.getStatsQueue()
	//						.addToStatsQueue(new KeyValueStatsInfo(category, getHostAddress(), command, timeTaken));
	//				ZMLibraryComponent.getStatsQueue()
	//						.addToStatsQueue(new KeyValueStatsInfo(category, APP_IP, command, timeTaken));
	//
	//				ZMLibraryComponent.getStatsQueue()
	//						.addToStatsQueue(new KeyValueStatsInfo(category, "GLOBAL", command, timeTaken));
	//
	//				ZMLibraryComponent.getStatsQueue().addToStatsQueue(new KeyValueStatsInfo(category,
	//						ZMLibraryComponent.getLibraryUtil().getProductionType(), command, timeTaken));
	//			}
	//		} catch (Exception e) {
	//			LOG.log(Level.INFO, "addToStats", new Object[]{"Exception while adding to Stats queue"});
	//		}
	//	}

	public SessionInterface getSessionInterface() {
		return sessionInterface;
	}
}
