//$Id$
package desksocket;

public final class StaticServer {
	
	private static final NettyServerHandlerAPI OBJ1;
	
	static
	{
	    OBJ1 = new NettyServerHandler();
	    System.out.println(OBJ1.toString()+ " Initialized");
	}
	
	public static NettyServerHandlerAPI getObj()
	{
	     return StaticServer.OBJ1;
	}

}
