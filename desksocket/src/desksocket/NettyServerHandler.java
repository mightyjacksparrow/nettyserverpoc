//$Id$
package desksocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.MessageLite;

import desksocket.RequestDataType.RequestOrigin;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class NettyServerHandler implements NettyServerHandlerAPI {

	private static final Map<ProtocolConstant, Server> PCVSSERVERMAP = new ConcurrentHashMap<>();

	@Override
	public void startServer(ServerInterface serverIf, SessionHandlerInterface seshandIf, ProtocolConstant pc)
			throws Exception {
		Server server = new Server(serverIf, seshandIf, pc);
		server.bind();
		server.start();
		PCVSSERVERMAP.put(pc, server);
	}

	@Override
	public void closeSession(long sessionId, ProtocolConstant pc, boolean flush) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		handler.getSession(sessionId).closeSession(flush);
	}

	@Override
	public InetSocketAddress getRemoteAddress(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getSession(sessionId).getSocketChannel().remoteAddress();
	}

	@Override
	public InetSocketAddress getLocalAddress(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getSession(sessionId).getSocketChannel().localAddress();
	}

	@Override
	public CustomOutputStream getOutputStream(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getSession(sessionId).getCustomOutputStream();
	}

	@Override
	public Object getProtocol(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getSession(sessionId).getProtocol();
	}

	@Override
	public void updateLastAccessTime(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		handler.getSession(sessionId).updateLastAccessTime();
	}

	@Override
	public long getCreationTime(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getSession(sessionId).getCreationTime();
	}

	@Override
	public void startTlsSupport(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		Session session = handler.getSession(sessionId);
		session.pendingList.clear();
		session.addSSLSupport(true);
	}

	@Override
	public void setSyncWrite(long sessionId, ProtocolConstant pc, boolean flag) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		handler.getSession(sessionId).setSyncWrite(flag);
	}

	@Override
	public void addCompressionSupport(Encoder encoder, long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		Session session = handler.getSession(sessionId);
		SocketChannel sc = session.getSocketChannel();
		// switch (encoder) {
		// case JZLIBEncoder:
		// sc.pipeline().addAfter(sc.pipeline().names().get(0),
		// JZlibEncoder.class.getName(),
		// new JZlibEncoder(ZlibWrapper.NONE));
		// break;
		// default:
		// return;
		// }
	}

	@Override
	public void addDecompressionSupport(Decoder decoder, long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		Session session = handler.getSession(sessionId);
		SocketChannel sc = session.getSocketChannel();
		// switch (decoder) {
		// case JZLIBDecoder:
		// sc.pipeline().addAfter(sc.pipeline().names().get(0),
		// JZlibDecoder.class.getName(),
		// new JZlibDecoder(ZlibWrapper.NONE));
		// break;
		// default:
		// break;
		// }
	}

	@Override
	public void addWebsocketHandlers(ProtocolConstant var1, long var2, WebSocketInterface wsIf) {
		SessionHandler var5 = ((Server) PCVSSERVERMAP.get(var1)).getSessionHandler();
		SocketChannel var6 = var5.getSession(var2).getSocketChannel();
		ChannelPipeline var7 = var6.pipeline();
		var7.addFirst(new ChannelHandler[] { new LoggingHandler() });
		var7.addAfter((String) var7.names().get(0), WebsocketCodec.class.getName(), new WebsocketCodec());
		//   System.out.println("adding WebSocketServerProtocolHandler with " + wsIf.getWSPath());
		var7.addAfter((String) var7.names().get(0), WebSocketServerProtocolHandler.class.getName(),
				new WebSocketServerProtocolHandler(wsIf.getWSPath()));
		var7.addAfter((String) var7.names().get(0), WebSocketFrameAggregator.class.getName(),
				new WebSocketFrameAggregator(500));//500bytes
		var7.addAfter((String) var7.names().get(0), PreHandshakeHandler.class.getName(), new PreHandshakeHandler(wsIf));
		var7.addAfter((String) var7.names().get(0), HttpObjectAggregator.class.getName(),
				new HttpObjectAggregator(20480));//20 kb is maximum size of HTTP object
		var7.addAfter((String) var7.names().get(0), HttpServerCodec.class.getName(), new HttpServerCodec());
	}

	@Override
	public void addProtoBufDecoder(long sessionId, ProtocolConstant pc, MessageLite msg) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		SocketChannel sc = handler.getSession(sessionId).getSocketChannel();

		ChannelPipeline pipeline = sc.pipeline();
		// adding first by default. Expose pipeline to user and ask user to add
		// handlers in future.
		pipeline.addFirst(new ProtobufDecoder(msg));
	}

	@Override
	public void removeProtBufDecoder(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		SocketChannel sc = handler.getSession(sessionId).getSocketChannel();

		ChannelPipeline pipeline = sc.pipeline();
		pipeline.remove(ProtobufDecoder.class);
	}

	@Override
	public void processReadMessages(long sessionId, ProtocolConstant pc, byte[] outcontent, RequestOrigin origin)
			throws IOException {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		if (handler.getSession(sessionId) == null) {
			// Happens in restart or socket disconnect, And in websocket case
			// old sessionId is passed
			// So discarding the packet
			return;
		}
		handler.getSession(sessionId).processReadMessages(outcontent, 1, origin);
	}

	@Override
	public SessionInterface getSession(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		if (handler.getSession(sessionId) == null) {
			return null;
		}
		return handler.getSession(sessionId).getSessionInterface();
	}

	@Override
	public void addHttpHandlers(long sessionId, ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();

		SocketChannel sc = handler.getSession(sessionId).getSocketChannel();
		ChannelPipeline pipeline = sc.pipeline();
		// if ssl handler added remove this logging handler
		// pipeline.addFirst(new LoggingHandler());
		pipeline.addAfter(pipeline.names().get(0), HttpContentCodec.class.getName(),
				new HttpContentCodec(sessionId, pc));
		pipeline.addAfter(pipeline.names().get(0), HttpServerCodec.class.getName(),
				new HttpServerCodec(4096, 8192, 16384));
	}

	@Override
	public Enumeration<Long> getAllCurrentSessions(ProtocolConstant pc) {
		SessionHandler handler = PCVSSERVERMAP.get(pc).getSessionHandler();
		return handler.getAllSessions();
	}

}
