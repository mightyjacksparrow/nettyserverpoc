//$Id$
package desksocket;

public class WSFrame implements ResponseObject  {

    private FrameType frametype;
    private byte[] content;
    private WSStatus status;
    
    public WSFrame(FrameType type, byte[] con) {
        this.frametype = type;
        this.content = con;
    }
    
    public void setStatus(WSStatus st) {
        this.status = st;
    }
    
    public WSStatus getStatus() {
        return this.status;
    }
    
    public FrameType getFrameType() {
        return this.frametype;
    }
    
    public byte[] getContent() {
        return this.content;
    }
    
    public int getLength() {
        return this.content.length;
    }

    public enum FrameType {
        CLOSE,PING,TEXT,BINARY
    }
}
