//$Id$
package desksocket;

public interface CacheUnit {
	int DBOP_INSERT = 100;
	int DBOP_UPDATE = 101;
	int DBOP_DELETE = 102;
	int DBOP_READ = 103;
	int DBOP_INSERT_CACHE_UPDATE = 104;

	int getObjectSize();

}
