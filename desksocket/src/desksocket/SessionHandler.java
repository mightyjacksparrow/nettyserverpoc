//$Id$
package desksocket;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.netty.channel.socket.SocketChannel;

public  class SessionHandler {

    private static final Logger LOG = Logger.getLogger(SessionHandler.class
            .getName());

    private ConcurrentHashMap<Long, Session> sessionMap = new ConcurrentHashMap<Long, Session>();

    private SessionHandlerInterface sessionHandlerInterface;
    private Cron cronInstance;

    public SessionHandler(SessionHandlerInterface sessionHandlerInterface) {
        this.sessionHandlerInterface = sessionHandlerInterface;
        cronInstance = new Cron();
        try {
        	//CRON job to keep polling once in a minute..to keep the Session Active
//            TaskScheduler.getInstance().scheduleRepetitive(cronInstance,
//                    ThreadPool.INMEMORY_SYSTEM, 0L,
//                    TimeUnit.MINUTES.toMillis(1));
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "INIT", e);
        }
    }

    public Long createSession(Server server, SocketChannel ch) throws Exception {
        Long sessionId = createSessionId(server);
        Session session = createSession(sessionId, server, ch);
        sessionMap.put(sessionId, session);
        session.initialize();
        return session.getSessionId();
    }

    public void removeSession(Long sessionId) {
        LOG.log(Level.FINE, "removeKey", new Object[] { sessionId });
        sessionMap.remove(sessionId);
        LOG.log(Level.FINE, "return", new Object[] { sessionId });
    }

    public Session getSession(Long sessionId) {
        return sessionMap.get(sessionId);
    }

    public Enumeration<Long> getAllSessions() {
        return sessionMap.keys();
    }

    public int getSessionsCount() {
        return sessionMap.size();
    }

    private Long createSessionId(Server server) throws Exception {
        ProtocolConstant protocol = server.getProtocolConstant();
        int protocolMask = protocol.getMask();

        return (SessionIDGenerator.getUniqueId() & getMask(protocolMask,
                protocol.isSecured()));
    }

    private Session createSession(Long sessionId, Server server,
            SocketChannel ch) throws Exception {
        return new Session(
                sessionHandlerInterface.createSessionInterfaceImpl(sessionId),
                sessionId, ch, server);
    }

    private long getMask(long protocol, boolean ssl) {
        if (!ssl) {
            return (protocol << 60) | 0x8fffffffffffffffL;
        } else {
            return (protocol << 60) | 0x0fffffffffffffffL;
        }
    }

    private class Cron implements Task {
        private Cron() {
        }

        private void reapConnection() {
            Session session;
            Long sessionId = null;
            Enumeration<Long> keys = getAllSessions();
            while (keys.hasMoreElements()) {
                sessionId = keys.nextElement();
                session = sessionId == null ? null : getSession(sessionId);

                if (session == null) {
                    continue;
                }
                Object token = null;
                try {
                    if (session.getUser() != null) {
                        token = ContextManager.getHandler().setUser(session.getUser());
                    }
                    boolean expired = session.isSessionExpired();
                    if (expired
                            || (System.currentTimeMillis() - session
                                    .getLastAccessTime()) > session.getTimeout()) {
                        // Session Idle for Idle timeout (or) Session expired set
                        // true
                        session.closeSession(expired ? false : true);
                        LOG.log(Level.INFO, "reapConnection",
                                new Object[] { sessionId + "Expired:" + expired });
                    }
                }
                finally {
                    if (token != null) {
                        ContextManager.getHandler().reset(token);
                    }
                }
            }
        }

        @Override
        public void process() {
            try {
                // Reap timed out sessions. Will be called for ever 1 minute
                reapConnection();
            } catch (Exception ex) {
                LOG.log(Level.INFO, "CRON RUN", ex);
            }
        }

        @Override
        public String getTaskName() {
            return "Netty_Cron";
        }
    }

}
