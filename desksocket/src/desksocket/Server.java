//$Id$
package desksocket;

import java.util.logging.Level;
import java.util.logging.Logger;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.internal.logging.JdkLoggerFactory;

public class Server {
	// https://github.com/netty/netty/wiki/User-guide-for-4.x
	// Check above link for eventloop and other definitions

	private static final Logger LOG = Logger.getLogger(Server.class.getName());
	private static final int BACKLOG = 200;

	/*
	 * backlog is usually described as the limit for the queue of incoming
	 * connections. See :
	 * http://veithen.github.io/2014/01/01/how-tcp-backlog-works-in-linux.html
	 */
	private static final PooledByteBufAllocator ALLOC = new PooledByteBufAllocator(true);
	private ServerInterface serverInterface;
	private SessionHandler sessionHandler = null;
	private ServerBootstrap bootStrap = null;
	private NioEventLoopGroup parentGroup;

	private NioEventLoopGroup childGroup;
	private AsyncRequestProcessor processor;
	private ProtocolConstant protocolConstant = null;

	/*
	 * TODO should add functionality for below flag.(i.e.)Rejecting connections
	 * This was missed out in the previous code
	 */
	private boolean acceptNewConn;

	static {
		io.netty.util.internal.logging.InternalLoggerFactory.setDefaultFactory(new JdkLoggerFactory());
	}

	public Server(ServerInterface serverInterface, SessionHandlerInterface sesHandlerIf, ProtocolConstant pc) {
		this.serverInterface = serverInterface;
		this.protocolConstant = pc;
		setSessionHandler(new SessionHandler(sesHandlerIf));
	}

	private void configureBootStrap() {
		bootStrap = new ServerBootstrap();
		parentGroup = new NioEventLoopGroup();
		childGroup = new NioEventLoopGroup();
		bootStrap.localAddress(protocolConstant.getPort());
		bootStrap.channel(NioServerSocketChannel.class);
		bootStrap.group(parentGroup, childGroup);
		bootStrap.childHandler(new MessageChannelInitializer());
		bootStrap.option(ChannelOption.SO_BACKLOG, BACKLOG);
		bootStrap.childOption(ChannelOption.AUTO_READ, true);
		bootStrap.childOption(ChannelOption.SO_KEEPALIVE, true);
		bootStrap.childOption(ChannelOption.ALLOCATOR, ALLOC);
	}

	public void bind() throws Exception {
		configureBootStrap();
		serverInterface.initialize();
	}

	public void start() throws Exception {
		bootStrap.bind().sync();
		LOG.log(Level.INFO, "Server started sucessfully server {0} ", new Object[] { getProtocolConstant().getName() });
	}

	public void stopServer() {
		try {
			parentGroup.shutdownGracefully().sync();
			childGroup.shutdownGracefully().sync();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setSessionHandler(SessionHandler sessionHandler) {
		this.sessionHandler = sessionHandler;
		this.processor = new AsyncRequestProcessor(sessionHandler, protocolConstant);
	}

	public void setProtocolConstant(ProtocolConstant protocolConstant) {
		this.protocolConstant = protocolConstant;
	}

	public void acceptNewConn(boolean acceptNewConn) {
		this.acceptNewConn = acceptNewConn;
	}

	public void process(RequestData pkt) {
		processor.process(pkt);
	}

	public AsyncRequestProcessor giveAsyncRequestProcessor() {
		return this.processor;
	}

	public SessionHandler getSessionHandler() {
		return this.sessionHandler;
	}

	public ProtocolConstant getProtocolConstant() {
		return this.protocolConstant;
	}

	private class MessageChannelInitializer extends ChannelInitializer<SocketChannel> {
		@Override
		protected void initChannel(SocketChannel sc) throws Exception {
			ChannelPipeline pipeLine = sc.pipeline();
			if (serverInterface.getProtocolDecoder() != null) {
			//	System.out.println("Added new Protocol Decoder to Pipeline-- Default Flow");
				pipeLine.addLast(new ProtocolDecoder(serverInterface.getProtocolDecoder()));
			}
			pipeLine.addLast(new MessageInboundHandler(Server.this));
		}
	}
}
