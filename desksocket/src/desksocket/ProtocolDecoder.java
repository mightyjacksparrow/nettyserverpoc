//$Id$
package desksocket;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

//Decodes bytebuf, as data will be sent as Bytes and not as Packets
public class ProtocolDecoder extends ByteToMessageDecoder {
    protected SessionHandler sessionHandler;
    protected Long sessionId;

    private ProtocolDecoderInterface pdInterface;
    private int before = 0;
    private boolean closed = false;
    private boolean mustParse = true;


    public ProtocolDecoder(ProtocolDecoderInterface pdInterface) {
        this.pdInterface = pdInterface;
    }
    
    /**
     * Decode the from one {@link ByteBuf} to an other. This method will be called till either the input
     * {@link ByteBuf} has nothing to read when return from this method or till nothing was read from the input
     * {@link ByteBuf}.
     */

    
//    In a stream-based transport such as TCP/IP, received data is stored into a socket receive buffer. 
//    Unfortunately, the buffer of a stream-based transport is not a queue of packets but a queue of bytes. 
//    It means, even if you sent two messages as two independent packets, an operating system will not treat them as two messages but as just a bunch of bytes. 
//    Therefore, there is no guarantee that what you read is exactly what your remote peer wrote.
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    		//System.out.println("ProtocolDecoder decode called");
        if (!closed) {
            sessionHandler.getSession(sessionId).updateLastAccessTime();
            while (in.readableBytes() > 0 && mustParse && sessionHandler.getSession(sessionId) != null) {
                sessionHandler.getSession(sessionId).addReadDataCount(in.readableBytes() - before);
                int prevSize = out.size();
                //assert out.size()->0as read events once they are put in the list
             //   System.out.println("ProtocolDecoder decode called prevSize " + prevSize + "  " + sessionId );
                pdInterface.parseContent(sessionId, new ByteBufWrapperImpl(in), out);//Just adding data to out here.
                before = in.readableBytes();
                if (out.size() == prevSize) {
                	   System.out.println("ProtocolDecoder decode called break ");
                }
                //assert out.size()->1 if object has been constructed
                if (out.size() > prevSize) { //a request object has been parsed.
                    boolean parseable = pdInterface.shouldEnableParse(out.get(0));
                 //   System.out.println("ProtocolDecoder decode shouldEnableParse called "+parseable);
                    setMustParse(parseable);
                }
            }
        }
    }

    @Override
    public void channelInactive (ChannelHandlerContext ctx) throws Exception {
    		//System.out.println("ProtocolDecoder channelInactive called");
        closed = true;
        super.channelInactive(ctx);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
     	//System.out.println("ProtocolDecoder userEventTriggered called");
        channelRead(ctx, ctx.alloc().buffer(0));
    }

    boolean getMustParse() {
        return this.mustParse;
    }

    void setMustParse(boolean mustParse) {
        this.mustParse = mustParse;
    }

    public void setSessionHandler(SessionHandler sessionHandler) {
        this.sessionHandler = sessionHandler;
    }

    public void setSessionId (Long sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
     * By default, bytes are continuously accumulated and parsed and sent as channelRead events.
     * Override this method if you want to stop parsing. Default to true. False if you want 
     * to stop parsing.
     * @param reqObject
     * @return
     */

}
