//$Id$
package desksocket;

import java.util.Arrays;

public class ResponseContent {

    private boolean isfinal = false;;
    private byte[] content ;
    
    public ResponseContent(byte[] content , int len, boolean isfinal) {
        this.content = Arrays.copyOf(content, len);
        this.isfinal = isfinal;
    }
 
    public byte[] getResponseContent() {
        return this.content;
    }

    public int getLength() {
        return this.content.length;
    }
    
    public boolean isFinal() {
        return this.isfinal;
    }
}
